﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;


namespace ePark.Services
{
   public class ContractService : IDisposable
    {
         private readonly EparkContext _db;
         public ContractService()
        {
            _db = new EparkContext();
        }

         public Contract GetById(int contractId)
         {
             // if not found will return null
             return _db.Contracts.FirstOrDefault(x => x.ContractID == contractId && x.ContractStatusID == 1);
         }
       
         public List<Contract> GetList()
         {
             // if not found will return null
             return _db.Contracts.ToList();
         }

        public List<Contract> GetListWithTransactionsByUserId(int id)
        {
            // if not found will return null
            return _db.Contracts.Where(x=> x.AspNetUserID == id).Include(x=> x.ContractTransactions).ToList();
        }
        

       public Contract GetByUserId(int id)
       {
           return _db.Contracts.FirstOrDefault(x => x.AspNetUserID == id && x.ContractStatusID == 1 && x.IsActive == true);
           
       }

         public int Create(Contract contract)
         {
             var newContract = new Contract
             {
                 AspNetUserID = contract.AspNetUserID,
                 EntityID = contract.EntityID,
                 CreateDate = contract.CreateDate,
                 IsActive = contract.IsActive,
                 ContractStatusID = contract.ContractStatusID,
                 GroupID = contract.GroupID

             };

             _db.Contracts.Add(newContract);

             _db.SaveChanges();

             return (int) newContract.ContractID;
         }

         public int Update(Contract contract)
         {
             var existingContract = _db.Contracts.FirstOrDefault(x => x.ContractID == contract.ContractID);

             if (existingContract != null)
             {
                  existingContract.AspNetUserID = contract.AspNetUserID;
                 existingContract.EntityID = contract.EntityID;
                 existingContract.CreateDate = contract.CreateDate;
                 existingContract.IsActive = contract.IsActive;
                 existingContract.ContractStatusID = contract.ContractStatusID;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public int Delete(Contract contract)
         {
             var existingContract = _db.Contracts.FirstOrDefault(x => x.ContractID == contract.ContractID);

             if (existingContract != null)
             {
                 _db.Contracts.Remove(contract);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int DeleteById(int transactionId)
         {
             var existingContractTransaction = _db.ContractTransactions.FirstOrDefault(x => x.ContractTransactionID == transactionId);

             if (existingContractTransaction != null)
             {
                 _db.ContractTransactions.Remove(existingContractTransaction);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }
    }
}
