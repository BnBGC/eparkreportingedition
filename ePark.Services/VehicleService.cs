﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class VehicleService : IDisposable
    {
                    
         private readonly EparkContext _db;
         public VehicleService()
        {
            _db = new EparkContext();
        }


         public List<Vehicle> GetByContract(int contractId)
         {
             // if not found will return null
             return _db.Vehicles.Where(x => x.ContractID == contractId).ToList();
         }


         public Vehicle GetById(int parkingVehicleId)
         {
             // if not found will return null
             return _db.Vehicles.FirstOrDefault(x => x.VehicleID == parkingVehicleId);
         }

         public List<Vehicle> GetList()
         {
             // if not found will return null
             return _db.Vehicles.ToList();
         }

         public int Create(Vehicle parkingVehicle)
         {
             var newParkingVehicle = new Vehicle
             {
                AspNetUserID = parkingVehicle.AspNetUserID,
                ContractID = parkingVehicle.ContractID,
                VehicleMake = parkingVehicle.VehicleMake,
                VehicleModel = parkingVehicle.VehicleModel,
                VehicleYear = parkingVehicle.VehicleYear,
                VehicleColor = parkingVehicle.VehicleColor,
                VehicleLicense = parkingVehicle.VehicleLicense,
                IsActive = parkingVehicle.IsActive
             };

             _db.Vehicles.Add(newParkingVehicle);

             return _db.SaveChanges();
         }

         public int Update(Vehicle parkingVehicle)
         {
             var existingParkingVehicle = _db.Vehicles.FirstOrDefault(x => x.VehicleID == parkingVehicle.VehicleID);

             if (existingParkingVehicle != null)
             {
                existingParkingVehicle.VehicleID = parkingVehicle.VehicleID;
                 existingParkingVehicle.ContractID = parkingVehicle.ContractID;
                existingParkingVehicle.AspNetUserID = parkingVehicle.AspNetUserID;
                existingParkingVehicle.VehicleMake = parkingVehicle.VehicleMake;
                existingParkingVehicle.VehicleModel = parkingVehicle.VehicleModel;
                existingParkingVehicle.VehicleYear = parkingVehicle.VehicleYear;
                existingParkingVehicle.VehicleColor = parkingVehicle.VehicleColor;
                existingParkingVehicle.VehicleLicense = parkingVehicle.VehicleLicense;
                existingParkingVehicle.IsActive = parkingVehicle.IsActive;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }
    }
}
