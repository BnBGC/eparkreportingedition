﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class PassServices : IDisposable
    {
        
         private readonly EparkContext _db;

         public PassServices()
        {
            _db = new EparkContext();
        }

        public List<Pass> GetAll()
         {
             // if not found will return null
            return _db.Passes.ToList();
         }

         public Pass GetById(int PassID)
         {
             // if not found will return null
             return _db.Passes.FirstOrDefault(x => x.PassId == PassID);
         }
        
         public Pass GetByNo(string PassNo)
         {
             // if not found will return null
             return _db.Passes.FirstOrDefault(x => x.PassNo == PassNo);
         }
        
         public List<Pass> GetByContractId(int ContractID)
         {
             // if not found will return null
             return _db.Passes.Where(x => x.ContractID == ContractID).ToList();
         }


         public int Create(Pass pass)
         {
             var newPass = new Pass
             {
                 PassNo = pass.PassNo,
                 ContractID = pass.ContractID,
                 ActiveDate = pass.ActiveDate,
                 InactiveDate = pass.InactiveDate,
                 IsActive = pass.IsActive
             };

             _db.Passes.Add(newPass);

             return _db.SaveChanges();
         }


         public int Update(Pass pass)
         {
             var existingPass = _db.Passes.FirstOrDefault(x => x.PassId == pass.PassId);

             if (existingPass != null)
             {
                 existingPass.PassNo = pass.PassNo;
                 existingPass.ContractID = pass.ContractID;
                 existingPass.ActiveDate = pass.ActiveDate;
                 existingPass.InactiveDate = pass.InactiveDate;
                 existingPass.IsActive = pass.IsActive;

                 return _db.SaveChanges();
             }

             return 0;
         }
         public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }

    }
}
