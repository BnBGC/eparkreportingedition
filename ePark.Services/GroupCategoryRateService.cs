﻿using System;
using System.Collections.Generic;
using System.Linq;
using ePark.Data;

namespace ePark.Services 
{
   public class GroupCategoryRateService : IDisposable
    {
        private readonly EparkContext _db;

        public GroupCategoryRateService()
        {
            _db = new EparkContext();
        }
              
        public List<vwGroupCategoryRate> GetList()
        {
            // if not found will return null
            return _db.vwGroupCategoryRates.ToList();
        }

        public  List<vwGroupCategoryRate> GetByCategoryId(int parkingCategoryId)
        {
            // if not found will return null
            List<vwGroupCategoryRate> vwgrpcatrate = new List<vwGroupCategoryRate>();
            try
            {
                vwgrpcatrate = _db.vwGroupCategoryRates.Where(x => x.GroupCategoryID == parkingCategoryId).ToList();
            }
            catch(Exception ex)
            {

                string strMsg = ex.Message.ToString();

            }

            return vwgrpcatrate;
        }


        public vwGroupCategoryRate GetByGroupId(int parkingGroupId)
        {
            // if not found will return null
            return _db.vwGroupCategoryRates.FirstOrDefault(x => x.GroupID == parkingGroupId);
        }
      
        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }

    
}
