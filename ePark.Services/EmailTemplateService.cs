﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class EmailTemplateService : IDisposable
    {
        private readonly EparkContext _db;

        public EmailTemplateService()
        {
            _db = new EparkContext();
        }


        public EmailTemplate GetById(int emailTempID)
        {
            // if not found will return null
            return _db.EmailTemplates.FirstOrDefault(x => x.EmailTemplateID == emailTempID);
        }

        public List<EmailTemplate> GetList()
        {
            return _db.EmailTemplates.ToList();
        }

        public int Create(EmailTemplate emailTemp)
        {
            var newEmailTemp = new EmailTemplate
            {
                EmailDesc = emailTemp.EmailDesc,
                EmailShortDesc = emailTemp.EmailShortDesc,
                EmailText = emailTemp.EmailText,
            };

            _db.EmailTemplates.Add(newEmailTemp);

            return _db.SaveChanges();
        }

        public int Update(EmailTemplate emailTemp)
        {
            var existingEmailTemp = _db.EmailTemplates.FirstOrDefault(x => x.EmailTemplateID == emailTemp.EmailTemplateID);

            if (existingEmailTemp != null)
            {
                existingEmailTemp.EmailTemplateID = emailTemp.EmailTemplateID;
                existingEmailTemp.EmailDesc = emailTemp.EmailDesc;
                existingEmailTemp.EmailText = emailTemp.EmailText;
                existingEmailTemp.EmailShortDesc = emailTemp.EmailShortDesc;

                return _db.SaveChanges();
            }

            return 0;
        }

        public int Delete(EmailTemplate emailTemp)
        {
            var existingEmailTemp = _db.EmailTemplates.FirstOrDefault(x => x.EmailTemplateID == emailTemp.EmailTemplateID);

            if (existingEmailTemp != null)
            {
                _db.EmailTemplates.Remove(existingEmailTemp);
                return _db.SaveChanges();
            }

            return 0;
        }

        public int DeleteById(int emailTransID)
        {
            var existingEmailTran = _db.EmailTrans.FirstOrDefault(x => x.EmailTransID == emailTransID);

            if (existingEmailTran != null)
            {
                _db.EmailTrans.Remove(existingEmailTran);
                return _db.SaveChanges();
            }

            return 0;
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }


    }
}
