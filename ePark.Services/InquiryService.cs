﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;


namespace ePark.Services
{
    public class InquiryService : IDisposable
    {

        private readonly EparkContext _db;
        public InquiryService()
        {
            _db = new EparkContext();
        }

        public Inquiry GetById(int parkingInquiryId)
        {
            return (_db.Inquiries.Where(x => x.InquiryID == parkingInquiryId)).First();
        }

        public List<Inquiry> GetList()
        {
            return _db.Inquiries.Include(x => x.AspNetUser).Include(x => x.Group).ToList();
        }

        public List<vwInquiry> GetViewList(int daysBack)
        {
            return _db.vwInquiries.Where(x => x.CreateDate > DbFunctions.AddDays(DateTime.Today, -daysBack)).ToList();
        }

        public List<vwInquiry> GetViewListByUserId(int userId)
        {
            return _db.vwInquiries.Where(x => x.AspNetUserProfilesId == userId).ToList();
        }

        public List<vwInquiry> GetViewListByUserId(int userId, int daysBack)
        {
            return _db.vwInquiries.Where(x => x.AspNetUserProfilesId == userId && x.CreateDate > DbFunctions.AddDays(DateTime.Today, -30)).ToList();
        }

        public int Create(Inquiry parkingInquiry)
        {
            var newParkingInquiry = new Inquiry
            {
                ASPNetUserId = parkingInquiry.ASPNetUserId,
                GroupID = 17, //ToDo: Create Unassigned with id of 0, Ask Rex
                ReservedMonth = parkingInquiry.ReservedMonth,
                ReservedMonthDateTime = parkingInquiry.ReservedMonthDateTime,
                CreateDate = DateTime.Now,
                Notes = parkingInquiry.Notes,
                IsApproved = false
            };

            _db.Inquiries.Add(newParkingInquiry);

            return _db.SaveChanges();
        }

        public int Update(Inquiry parkingInquiry)
        {
            var existingParkingInquiry = _db.Inquiries.FirstOrDefault(x => x.InquiryID == parkingInquiry.InquiryID);

            if (existingParkingInquiry != null)
            {
                existingParkingInquiry.ASPNetUserId = parkingInquiry.ASPNetUserId;
                existingParkingInquiry.GroupID = parkingInquiry.GroupID;
                existingParkingInquiry.CreateDate = parkingInquiry.CreateDate;
                existingParkingInquiry.ReservedMonth = parkingInquiry.ReservedMonth;
                existingParkingInquiry.ReservedMonthDateTime = parkingInquiry.ReservedMonthDateTime;
                existingParkingInquiry.Notes = parkingInquiry.Notes;
                existingParkingInquiry.ApprovedDate = parkingInquiry.ApprovedDate;
                existingParkingInquiry.IsApproved = parkingInquiry.IsApproved;
                
                return _db.SaveChanges();
            }

            return 0;
        }


        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
