﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ePark.Data;

namespace ePark.Services 
{
    public class UserContractTransactionService : IDisposable
    {
        private readonly EparkContext _db;

        public UserContractTransactionService()
        {
            _db = new EparkContext();
        }

        public List<vwUserContractTransaction> GetTrans()
        {
            // if not found will return null
            return _db.vwUserContractTransactions.ToList();
        }

        public List<vwUserContractTransaction> GetUnPaidContractTrans()
        {
            // if not found will return null
            return _db.vwUserContractTransactions.Where(x => x.Paid == false).ToList();
        }
        
        public List<vwUserContractTransaction> GetPaidContractTrans()
        {
            // if not found will return null
            return _db.vwUserContractTransactions.Where(x => x.Paid == true).ToList();
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
