﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ePark.Data;

namespace ePark.Services
{
    public class PaymentTransactionLogsService : IDisposable
    {
        
         private readonly EparkContext _db;
         public PaymentTransactionLogsService()
        {
            _db = new EparkContext();
        }

         public PaymentTransactionLog GetById(int translogId)
         {
             // if not found will return null
             return _db.PaymentTransactionLogs.FirstOrDefault(x => x.transLogID == translogId);
         }

         public List<PaymentTransactionLog> GetList()
         {
             // if not found will return null
             return _db.PaymentTransactionLogs.ToList();
         }

         public int Create(PaymentTransactionLog paymenttrans)
         {
             var newPayTrans = new PaymentTransactionLog
             {
                    transLogID = paymenttrans.transLogID,
	                transCardType = paymenttrans.transCardType,
	                transRequestType = paymenttrans.transRequestType,
	                transDate = paymenttrans.transDate,
	                transMerchantCode = paymenttrans.transMerchantCode,
	                transSettleCode = paymenttrans.transSettleCode,
	                transLast5Digits = paymenttrans.transLast5Digits,
	                transAmount = paymenttrans.transAmount,
	                transConvFee = paymenttrans.transConvFee,
	                transExpMM = paymenttrans.transExpMM,
	                transExpYY = paymenttrans.transExpYY,
	                transBillAddressSent = paymenttrans.transBillAddressSent,
	                transCardSecurityValue = paymenttrans.transCardSecurityValue,
	                transDataSent = paymenttrans.transDataSent,
	                transReturnCode = paymenttrans.transReturnCode,
                    transResponseString = paymenttrans.transResponseString,
	                transTransactionID = paymenttrans.transTransactionID,
	                transDateStamp = paymenttrans.transDateStamp,
	                transAuthorization = paymenttrans.transAuthorization,
	                transStatus = paymenttrans.transStatus,
	                transUser1 = paymenttrans.transUser1,
	                transUser2 = paymenttrans.transUser2,
	                transUser3  = paymenttrans.transUser3,
	                transQuery = paymenttrans.transQuery,
	                TransReceiptID = paymenttrans.TransReceiptID,
                    UserId = paymenttrans.UserId
             };

             _db.PaymentTransactionLogs.Add(newPayTrans);

             return _db.SaveChanges();
         }

         public int Update(PaymentTransactionLog PayTrans)
         {
             var existingPayTrans = _db.PaymentTransactionLogs.FirstOrDefault(x => x.transLogID == PayTrans.transLogID);

             if (existingPayTrans != null)
             {

                 existingPayTrans.transLogID = PayTrans.transLogID;
                 existingPayTrans.transCardType = PayTrans.transCardType;
                 existingPayTrans.transRequestType = PayTrans.transRequestType;
                 existingPayTrans.transDate = PayTrans.transDate;
                 existingPayTrans.transMerchantCode = PayTrans.transMerchantCode;
                 existingPayTrans.transSettleCode = PayTrans.transSettleCode;
                 existingPayTrans.transLast5Digits = PayTrans.transLast5Digits;
                 existingPayTrans.transAmount = PayTrans.transAmount;
                 existingPayTrans.transConvFee = PayTrans.transConvFee;
                 existingPayTrans.transExpMM = PayTrans.transExpMM;
                 existingPayTrans.transExpYY = PayTrans.transExpYY;
                 existingPayTrans.transBillAddressSent = PayTrans.transBillAddressSent;
                 existingPayTrans.transCardSecurityValue = PayTrans.transCardSecurityValue;
                 existingPayTrans.transDataSent = PayTrans.transDataSent;
                 existingPayTrans.transReturnCode = PayTrans.transReturnCode;
                 existingPayTrans.transResponseString = PayTrans.transResponseString;
                 existingPayTrans.transTransactionID = PayTrans.transTransactionID;
                 existingPayTrans.transDateStamp = PayTrans.transDateStamp;
                 existingPayTrans.transAuthorization = PayTrans.transAuthorization;
                 existingPayTrans.transStatus = PayTrans.transStatus;
                 existingPayTrans.transUser1 = PayTrans.transUser1;
                 existingPayTrans.transUser2 = PayTrans.transUser2;
                 existingPayTrans.transUser3 = PayTrans.transUser3;
                 existingPayTrans.transQuery = PayTrans.transQuery;
                 existingPayTrans.TransReceiptID = PayTrans.TransReceiptID;
                 existingPayTrans.UserId = PayTrans.UserId;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public int Delete(PaymentTransactionLog payTrans)
         {
             var existingContract = _db.PaymentTransactionLogs.FirstOrDefault(x => x.transLogID == payTrans.transLogID);

             if (existingContract != null)
             {
                 _db.PaymentTransactionLogs.Remove(payTrans);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int DeleteById(int transLogId)
         {
             var existingPaymentTransactionLog = _db.PaymentTransactionLogs.FirstOrDefault(x => x.transLogID == transLogId);

             if (existingPaymentTransactionLog != null)
             {
                 _db.PaymentTransactionLogs.Remove(existingPaymentTransactionLog);
                 return _db.SaveChanges();
             }

             return 0;
         }
        
        public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }
    }
}
