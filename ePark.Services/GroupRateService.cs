﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class GroupRateService : IDisposable
    {
        private readonly EparkContext _db;
         public GroupRateService()
        {
            _db = new EparkContext();
        }
        
         public List<GroupRate> GetList()
         {
             // if not found will return null
             return _db.GroupRates.ToList();
         }


         public GroupRate GetByGroupRateId(int grouprateId)
         {
             // if not found will return null
             return _db.GroupRates.FirstOrDefault(x => x.GroupRatesID == grouprateId);
         }

         public GroupRate GetByGroupId(int groupId)
         {
             // if not found will return null
             return _db.GroupRates.FirstOrDefault(x => x.GroupID == groupId);
         }

         public int Update(GroupRate grouprate)
         {
             var existingGroupRate = _db.GroupRates.FirstOrDefault(x => x.GroupRatesID == grouprate.GroupRatesID);

             if (existingGroupRate != null)
             {
                 existingGroupRate.GroupRatesID = grouprate.GroupRatesID;
                 existingGroupRate.GroupID = grouprate.GroupID;
                 existingGroupRate.Amount = grouprate.Amount;
                 existingGroupRate.GroupRateBegin = grouprate.GroupRateBegin;
                 existingGroupRate.GroupRateEnd = grouprate.GroupRateEnd;
                 existingGroupRate.IsActive = grouprate.IsActive;

                 return _db.SaveChanges();
             }

             return 0;
         }
        
         public int Create(GroupRate groupRate)
         {
             var newGroupRate = new GroupRate
             {
                 GroupID = groupRate.GroupID,
                 Amount = groupRate.Amount, //ToDo: Create Unassigned with id of 0, Ask Rex
                 IsActive = groupRate.IsActive,
                 GroupRateBegin = groupRate.GroupRateBegin,
                 GroupRateEnd = groupRate.GroupRateEnd
             };

             _db.GroupRates.Add(newGroupRate);

             return _db.SaveChanges();
         }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }

    }
}
