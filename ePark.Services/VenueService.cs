﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class VenueService : IDisposable
    {
        private readonly EparkContext _db;
        public VenueService()
        {
            _db = new EparkContext();
        }

        public Venue GetById(int venueId)
        {
            // if not found will return null
            return _db.Venues.FirstOrDefault(x => x.VenueID == venueId);
        }

        public List<Venue> GetList()
        {
            return _db.Venues.ToList();
        }

        public int Create(Venue venue)
        {
            var newVenue = new Venue
            {
                EntityID = venue.EntityID,
                VenueDesc = venue.VenueDesc,
                CreatedDate = venue.CreatedDate,
                IsActive = venue.IsActive
            };

            _db.Venues.Add(newVenue);

            return _db.SaveChanges();
        }

        public int Update(Venue venue)
        {
            var existingVenue = _db.Venues.FirstOrDefault(x => x.VenueID == venue.VenueID);

            if (existingVenue != null)
            {
                existingVenue.EntityID = venue.EntityID;
                existingVenue.VenueDesc = venue.VenueDesc;
                existingVenue.CreatedDate = venue.CreatedDate;
                existingVenue.IsActive = venue.IsActive;

                return _db.SaveChanges();
            }

            return 0;
        }

        public int Delete(Venue venue)
        {
            var existingVenue = _db.Venues.FirstOrDefault(x => x.VenueID == venue.VenueID);

            if (existingVenue != null)
            {
                _db.Venues.Remove(existingVenue);
                return _db.SaveChanges();
            }

            return 0;
        }

        public int DeleteById(int venueId)
        {
            var existingVenue = _db.Venues.FirstOrDefault(x => x.VenueID == venueId);

            if (existingVenue != null)
            {
                _db.Venues.Remove(existingVenue);
                return _db.SaveChanges();
            }

            return 0;
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
