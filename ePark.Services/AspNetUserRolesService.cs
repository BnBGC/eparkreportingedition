﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class AspNetUserRolesService : IDisposable
    {

        private readonly EparkContext _db;

           public AspNetUserRolesService()
        {
            _db = new EparkContext();
        }

           public AspNetUserRole GetByUserId(int id)
           {
               // if not found will return null
               return _db.AspNetUserRoles.FirstOrDefault(x => x.UserId == id);
           }


        
           public List<AspNetUserRole> GetUsersByRole(int id)
           {
               // if not found will return null
               return _db.AspNetUserRoles.Where(x => x.RoleId == id).ToList();
           }

           public int Create(AspNetUserRole aspNetUserRole)
           {
               var newaspNetUserRole = new AspNetUserRole
               {
                   UserId = aspNetUserRole.UserId,
                   RoleId = aspNetUserRole.RoleId
               };

               _db.AspNetUserRoles.Add(newaspNetUserRole);

               return _db.SaveChanges();
           }


           public int Update(AspNetUserRole aspNetUserRole)
           {
               var existingAspNetUserRole = _db.AspNetUserRoles.FirstOrDefault(x => x.UserId == aspNetUserRole.UserId && x.RoleId == aspNetUserRole.RoleId);

               if (existingAspNetUserRole != null)
               {

                   existingAspNetUserRole.UserId = aspNetUserRole.UserId;
                   existingAspNetUserRole.RoleId = aspNetUserRole.RoleId;
                  
                   return _db.SaveChanges();
               }

               return 0;
           }

           public int Delete(AspNetUserRole aspNetUserRole)
           {
               var existingaspNetUserRole = _db.AspNetUserRoles.FirstOrDefault(x => x.UserId == aspNetUserRole.UserId && x.RoleId == aspNetUserRole.RoleId);

               if (existingaspNetUserRole != null)
               {
                   _db.AspNetUserRoles.Remove(aspNetUserRole);
                   return _db.SaveChanges();
               }

               return 0;
           }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
