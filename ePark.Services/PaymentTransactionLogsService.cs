﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class PaymentTransactionLogsService : IDisposable
    {
         private readonly eParkDb _db;
         public PaymentTransactionLogsService()
        {
            _db = new eParkDb();
        }

         public PaymentTransactionLog GetById(int transLogID)
         {
             // if not found will return null
             return _db.PaymentTransactionLogs.FirstOrDefault(x => x.transLogID == transLogID);
         }


         public List<PaymentTransactionLog> GetList()
         {
             // if not found will return null
             return _db.PaymentTransactionLogs.ToList();
         }

         public int Create(PaymentTransactionLog paymentTransactionLogs)
         {
             var newPaymentTransactionLog = new PaymentTransactionLog
             {
                transCardType = paymentTransactionLogs.transCardType,
                transRequestType = paymentTransactionLogs.transRequestType,
                transDate = paymentTransactionLogs.transDate,
                transMerchantCode = paymentTransactionLogs.transMerchantCode,
                transSettleCode = paymentTransactionLogs.transSettleCode,
                transLast5Digits = paymentTransactionLogs.transLast5Digits,
                transAmount = paymentTransactionLogs.transAmount,
                transConvFee = paymentTransactionLogs.transConvFee,
                transExpMM = paymentTransactionLogs.transExpMM,
                transExpYY = paymentTransactionLogs.transExpYY,
                transBillAddressSent = paymentTransactionLogs.transBillAddressSent,
                transCardSecurityValue = paymentTransactionLogs.transCardSecurityValue,
                transDataSent = paymentTransactionLogs.transDataSent,
                transReturnCode = paymentTransactionLogs.transReturnCode,
                transResponseString = paymentTransactionLogs.transResponseString,
                transTransactionID = paymentTransactionLogs.transTransactionID,
                transDateStamp = paymentTransactionLogs.transDateStamp,
                transAuthorization = paymentTransactionLogs.transAuthorization,
                transStatus = paymentTransactionLogs.transStatus,
                transUser1 = paymentTransactionLogs.transUser1,
                transUser2 = paymentTransactionLogs.transUser2,
                transUser3 = paymentTransactionLogs.transUser3,
                transQuery = paymentTransactionLogs.transQuery,
                TransReceiptID = paymentTransactionLogs.TransReceiptID
             };

             _db.PaymentTransactionLogs.Add(newPaymentTransactionLog);

             return _db.SaveChanges();
         }

         public int Update(PaymentTransactionLog paymentTransactionLogs)
         {
             var existingPaymentTransactionLog = _db.PaymentTransactionLogs.FirstOrDefault(x => x.transLogID == paymentTransactionLogs.transLogID);

             if (existingPaymentTransactionLog != null)
             {
                 existingPaymentTransactionLog.transLogID = paymentTransactionLogs.transLogID;
                existingPaymentTransactionLog.transCardType = paymentTransactionLogs.transCardType;
                existingPaymentTransactionLog.transRequestType = paymentTransactionLogs.transRequestType;
                existingPaymentTransactionLog.transDate = paymentTransactionLogs.transDate;
                existingPaymentTransactionLog.transMerchantCode = paymentTransactionLogs.transMerchantCode;
                existingPaymentTransactionLog.transSettleCode = paymentTransactionLogs.transSettleCode;
                existingPaymentTransactionLog.transLast5Digits = paymentTransactionLogs.transLast5Digits;
                existingPaymentTransactionLog.transAmount = paymentTransactionLogs.transAmount;
                existingPaymentTransactionLog.transConvFee = paymentTransactionLogs.transConvFee;
                existingPaymentTransactionLog.transExpMM = paymentTransactionLogs.transExpMM;
                existingPaymentTransactionLog.transExpYY = paymentTransactionLogs.transExpYY;
                existingPaymentTransactionLog.transBillAddressSent = paymentTransactionLogs.transBillAddressSent;
                existingPaymentTransactionLog.transCardSecurityValue = paymentTransactionLogs.transCardSecurityValue;
                existingPaymentTransactionLog.transDataSent = paymentTransactionLogs.transDataSent;
                existingPaymentTransactionLog.transReturnCode = paymentTransactionLogs.transReturnCode;
                existingPaymentTransactionLog.transResponseString = paymentTransactionLogs.transResponseString;
                existingPaymentTransactionLog.transTransactionID = paymentTransactionLogs.transTransactionID;
                existingPaymentTransactionLog.transDateStamp = paymentTransactionLogs.transDateStamp;
                existingPaymentTransactionLog.transAuthorization = paymentTransactionLogs.transAuthorization;
                existingPaymentTransactionLog.transStatus = paymentTransactionLogs.transStatus;
                existingPaymentTransactionLog.transUser1 = paymentTransactionLogs.transUser1;
                existingPaymentTransactionLog.transUser2 = paymentTransactionLogs.transUser2;
                existingPaymentTransactionLog.transUser3 = paymentTransactionLogs.transUser3;
                existingPaymentTransactionLog.transQuery = paymentTransactionLogs.transQuery;
                existingPaymentTransactionLog.TransReceiptID = paymentTransactionLogs.TransReceiptID;
                 return _db.SaveChanges();
             }

             return 0;
         }

         public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }
    }
}
