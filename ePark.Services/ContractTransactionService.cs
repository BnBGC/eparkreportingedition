﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class ContractTransactionService : IDisposable
    {
         private readonly EparkContext _db;
         public ContractTransactionService()
        {
            _db = new EparkContext();
        }


         public ContractTransaction GetById(int parkingTransactionId)
         {
             // if not found will return null
             return _db.ContractTransactions.FirstOrDefault(x => x.ContractTransactionID == parkingTransactionId);
         }

         public ContractTransaction GetDepositforContract(int contractid)
         {
             // if not found will return null
             return _db.ContractTransactions.FirstOrDefault(x => x.ContractID == contractid && x.ContractTransactionTypeID == 2);
         }

         public ContractTransaction GetPaymentforContract(int contractid)
         {
             // if not found will return null
             return _db.ContractTransactions.FirstOrDefault(x => x.ContractID == contractid && x.ContractTransactionTypeID == 1);
         }

         public List<ContractTransaction> GetList()
         {
             // if not found will return null
             return _db.ContractTransactions.ToList();
         }

         public int Create(ContractTransaction contractTransaction)
         {
             var newContract = new ContractTransaction
             {
                UnitID = contractTransaction.UnitID,
                ContractRatesID = contractTransaction.ContractRatesID,
                Paid = contractTransaction.Paid,
                Amount = contractTransaction.Amount,
                ReservedMonth =contractTransaction.ReservedMonth,
                ReservedMonthDateTime = contractTransaction.ReservedMonthDateTime,
                CreatedDate = contractTransaction.CreatedDate,
                ContractID = contractTransaction.ContractID,
                ContractTransactionTypeID = contractTransaction.ContractTransactionTypeID,
                GroupID = contractTransaction.GroupID
                
             };

             _db.ContractTransactions.Add(newContract);

             return _db.SaveChanges();
         }

         public int Update(ContractTransaction contractTransaction)
         {
             var existingContractTransaction = _db.ContractTransactions.FirstOrDefault(x => x.ContractTransactionID == contractTransaction.ContractTransactionID);

             if (existingContractTransaction != null)
             {
                existingContractTransaction.UnitID = contractTransaction.UnitID;
                existingContractTransaction.ContractRatesID = contractTransaction.ContractRatesID;
                existingContractTransaction.Paid = contractTransaction.Paid;
                existingContractTransaction.Amount = contractTransaction.Amount;
                existingContractTransaction.ReservedMonth =contractTransaction.ReservedMonth;
                existingContractTransaction.ReservedMonthDateTime = contractTransaction.ReservedMonthDateTime;
                existingContractTransaction.CreatedDate = contractTransaction.CreatedDate;
                 existingContractTransaction.ContractTransactionTypeID = contractTransaction.ContractTransactionTypeID;
                 existingContractTransaction.GroupID = contractTransaction.GroupID;
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int Delete(ContractTransaction contractTransaction)
         {
             var existingContractTransaction = _db.ContractTransactions.FirstOrDefault(x => x.ContractTransactionID == contractTransaction.ContractTransactionID);

             if (existingContractTransaction != null)
             {
                 _db.ContractTransactions.Remove(existingContractTransaction);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int DeleteById(int parkingTransactionId)
         {
             var existingContractTransaction = _db.ContractTransactions.FirstOrDefault(x => x.ContractTransactionID == parkingTransactionId);

             if (existingContractTransaction != null)
             {
                 _db.ContractTransactions.Remove(existingContractTransaction);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }

    }
}
