﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class ParticipantServices : IDisposable
    {
         private readonly EparkContext _db;

         public ParticipantServices()
        {
            _db = new EparkContext();
        }

         public List<Participant> GetByContract(int contractId)
         {
             // if not found will return null
             return _db.Participants.Where(x => x.ContractID == contractId).ToList();
         }


        public List<Participant> GetAll()
         {
             // if not found will return null
            return _db.Participants.ToList();
         }

         public Participant GetById(int participantID)
         {
             // if not found will return null
             return _db.Participants.FirstOrDefault(x => x.ParticipantID == participantID);
         }

         public List<Participant> GetByContractId(int contractId)
         {
             // if not found will return null
             return _db.Participants.Where(x => x.ContractID == contractId).Include(p => p.Contract).ToList();
         }


         public int Create(Participant participant)
         {
             var newparticipant = new Participant
             {
                 ContractID = participant.ContractID,
                 FirstName = participant.FirstName,
                 LastName = participant.LastName,
                 Relation = participant.Relation,
                 IsActive = participant.IsActive
             };

             _db.Participants.Add(newparticipant);

             return _db.SaveChanges();
         }


         public int Update(Participant participant)
         {
             var existingparticipant = _db.Participants.FirstOrDefault(x => x.ParticipantID == participant.ParticipantID);

             if (existingparticipant != null)
             {
                 existingparticipant.ContractID = participant.ContractID;
                 existingparticipant.FirstName = participant.FirstName;
                 existingparticipant.LastName = participant.LastName;
                 existingparticipant.Relation = participant.Relation;
                 existingparticipant.IsActive = participant.IsActive;

                 return _db.SaveChanges();
             }

             return 0;
         }
         public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }

    }
}
