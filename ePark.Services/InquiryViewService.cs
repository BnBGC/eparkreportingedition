﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ePark.Data;

namespace ePark.Services
{
    public class InquiryViewService : IDisposable
    {
         private readonly EparkContext _db;
         public InquiryViewService()
        {
            _db = new EparkContext();
        }

        public vwInquiry GetByInqId(int inqId)
        {

            return _db.vwInquiries.FirstOrDefault(x => x.InquiryID == inqId);

        }

        public vwInquiry GetByUserId(int userId)
         {
             // if not found will return null
             return _db.vwInquiries.FirstOrDefault(x => x.Id == userId);
         }

         public List<vwInquiry> GetList()
         {
             // if not found will return null
             return _db.vwInquiries.ToList();
         }

         public List<vwInquiry> GetImmediateInquiries(int days)
         {
             return _db.vwInquiries.Where(pi => DbFunctions.TruncateTime(DateTime.Now) >= DbFunctions.AddDays(pi.CreateDate, days) && pi.IsApproved == false).ToList();
         }

         public List<vwInquiry> GetRecentInquiries(int days)
         {
             return _db.vwInquiries.Where(pi => DbFunctions.TruncateTime(pi.CreateDate) < DbFunctions.AddDays(DbFunctions.TruncateTime(pi.CreateDate), days) && pi.IsApproved == false).ToList();
         }

                public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }


    }
}
