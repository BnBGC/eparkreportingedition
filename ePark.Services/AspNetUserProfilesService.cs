﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class AspNetUserProfilesService : IDisposable
    {
        
         private readonly EparkContext _db;

         public AspNetUserProfilesService()
        {
            _db = new EparkContext();
        }

         public AspNetUserProfile GetById(int id)
         {
             // if not found will return null
             return _db.AspNetUserProfiles.FirstOrDefault(x => x.Id == id);
         }

         public List<AspNetUserProfile> GetList()
         {
             return _db.AspNetUserProfiles.ToList();
         }
         public int Create(AspNetUserProfile aspNetUserProfile)
         {
             var newaspNetUserProfile = new AspNetUserProfile
             {
                 Id = aspNetUserProfile.Id,
                 Address1 = aspNetUserProfile.Address1,
	             Address2 = aspNetUserProfile.Address2,
	             City = aspNetUserProfile.City,
	             State = aspNetUserProfile.State,
	             PostalCode = aspNetUserProfile.PostalCode,
	             PhonePrimary = aspNetUserProfile.PhonePrimary,
	             PhoneSecondary = aspNetUserProfile.PhoneSecondary,
	             MccEmployee = aspNetUserProfile.MccEmployee,
	             CityEmployee = aspNetUserProfile.CityEmployee

             };

             _db.AspNetUserProfiles.Add(newaspNetUserProfile);

             return _db.SaveChanges();
         }

         public int Update(AspNetUserProfile aspNetUserProfile)
         {
             var existingAspNetUserProfile = _db.AspNetUserProfiles.FirstOrDefault(x => x.Id == aspNetUserProfile.Id);

             if (existingAspNetUserProfile != null)
             {
                 existingAspNetUserProfile.Address1 = aspNetUserProfile.Address1;
                 existingAspNetUserProfile.Address2 = aspNetUserProfile.Address2;
                 existingAspNetUserProfile.City = aspNetUserProfile.City;
                 existingAspNetUserProfile.State = aspNetUserProfile.State;
                 existingAspNetUserProfile.PostalCode = aspNetUserProfile.PostalCode;
                 existingAspNetUserProfile.PhoneSecondary = aspNetUserProfile.PhoneSecondary;
                 existingAspNetUserProfile.MccEmployee = aspNetUserProfile.MccEmployee;
                 existingAspNetUserProfile.CityEmployee = aspNetUserProfile.CityEmployee;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public int Delete(AspNetUserProfile aspNetUserProfile)
         {
             var existingAspNetUserProfiles = _db.AspNetUserProfiles.FirstOrDefault(x => x.Id == aspNetUserProfile.Id);

             if (existingAspNetUserProfiles != null)
             {
                 _db.AspNetUserProfiles.Remove(existingAspNetUserProfiles);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int DeleteById(int aspNetUserProfileID)
         {
             var existingAspNetUserProfiles = _db.AspNetUserProfiles.FirstOrDefault(x => x.Id == aspNetUserProfileID);

             if (existingAspNetUserProfiles != null)
             {
                 _db.AspNetUserProfiles.Remove(existingAspNetUserProfiles);
                 return _db.SaveChanges();
             }

             return 0;
         }


        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }


    }
}
