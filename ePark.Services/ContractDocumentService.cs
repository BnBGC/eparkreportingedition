﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class ContractDocumentService : IDisposable
    {
        private readonly EparkContext _db;
        public ContractDocumentService()
        {
            _db = new EparkContext();
        }

        public ContractDocument GetById(int documentContractId)
        {
            // if not found will return null
            return _db.ContractDocuments.FirstOrDefault(x => x.ContractDocumentID == documentContractId);
        }


        public List<ContractDocument> GetByContractId(int ContractId)
        {
            // if not found will return null
            return _db.ContractDocuments.Where(x => x.ContractID == ContractId).ToList();
        }


        public ContractDocument GetByFileName(string filename)
        {
            // if not found will return null
            return _db.ContractDocuments.FirstOrDefault(x => x.ContractFile == filename);
        }

        public List<ContractDocument> GetList()
        {
            // if not found will return null
            return _db.ContractDocuments.ToList();
        }

        public int Create(ContractDocument contractDocument)
        {
            var newParkingContractDocument = new ContractDocument
            {
                ContractFilePath = contractDocument.ContractFilePath,
                ContractFile = contractDocument.ContractFile,
                CreateDate = contractDocument.CreateDate,
                IsActive = contractDocument.IsActive
            };

            _db.ContractDocuments.Add(newParkingContractDocument);
            
            return _db.SaveChanges();
        }

        public int Update(ContractDocument contractDocument)
        {
            var existingParkingContractDocument = _db.ContractDocuments.FirstOrDefault(x => x.ContractDocumentID == contractDocument.ContractDocumentID);

            if (existingParkingContractDocument != null)
            {
                existingParkingContractDocument.ContractFilePath = contractDocument.ContractFilePath;
                existingParkingContractDocument.ContractFile = contractDocument.ContractFile;
                existingParkingContractDocument.CreateDate = contractDocument.CreateDate;
                existingParkingContractDocument.IsActive = contractDocument.IsActive;

                return _db.SaveChanges();
            }

            return 0;
        }

        public int Delete(ContractDocument parkingContract)
        {
            var existingParkingContractDocument = _db.ContractDocuments.FirstOrDefault(x => x.ContractDocumentID == parkingContract.ContractDocumentID);

            if (existingParkingContractDocument != null)
            {
                _db.ContractDocuments.Remove(existingParkingContractDocument);
                return _db.SaveChanges();
            }

            return 0;
        }

       

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
