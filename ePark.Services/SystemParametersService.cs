﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ePark.Data;

namespace ePark.Services
{
    public class SystemParametersService : IDisposable
    {

        private readonly EparkContext _db;

        public SystemParametersService()
        {
            _db = new EparkContext();
        }


        public List<SystemParameter> GetList()
        {
            // if not found will return null
            return _db.SystemParameters.ToList();
        }

        public SystemParameter GetByDesc(string desc)
        {
            // if not found will return null
            return _db.SystemParameters.FirstOrDefault(x => x.ParmDesc.Trim() == desc.Trim());
        }

        public int Create(SystemParameter SysParm)
        {
            var newSystemParameter = new SystemParameter
            {
                ParmDesc = SysParm.ParmDesc,
                ParmMoney = SysParm.ParmMoney,
                ParmRate = SysParm.ParmRate,
                ParmValue = SysParm.ParmValue
            };

            _db.SystemParameters.Add(newSystemParameter);

            return _db.SaveChanges();
        }

        public int Update(SystemParameter SysParm)
        {
            var existingSysParm = _db.SystemParameters.FirstOrDefault(x => x.ParametersID == SysParm.ParametersID);

            if (existingSysParm != null)
            {
                existingSysParm.ParametersID = SysParm.ParametersID;
                existingSysParm.ParmDesc = SysParm.ParmDesc;
                existingSysParm.ParmMoney = SysParm.ParmMoney;
                existingSysParm.ParmRate = SysParm.ParmRate;
                existingSysParm.ParmValue = SysParm.ParmValue;

                return _db.SaveChanges();
            }

            return 0;
        }

        public decimal TaxCalc(decimal? amt)
        {
            decimal taxrate = 0;
            var txrate = Math.Round(Convert.ToDecimal((GetByDesc("TaxRate").ParmRate * amt)),2,MidpointRounding.AwayFromZero);
            if (txrate != null)
                taxrate = (decimal)txrate;
            
            return taxrate;
        }

        public decimal CBIDCalc(decimal? amt)
        {
            decimal cbidrate = 0;
            var cbid = Math.Round(Convert.ToDecimal((GetByDesc("CBIDRate").ParmRate * amt)), 2, MidpointRounding.AwayFromZero);
            if (cbid != null)
                cbidrate = (decimal)cbid;

            return cbidrate;
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }



    }
}
