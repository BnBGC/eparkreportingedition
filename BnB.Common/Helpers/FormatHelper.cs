﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB.Common.Helpers
{
    public class FormatHelper
    {
        public static string ConvertToTitleCase(string naturalText)
        {
            var textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(naturalText);
        }
    }
}
