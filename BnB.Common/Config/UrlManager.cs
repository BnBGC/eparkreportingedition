﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB.Common.Config
{
    class UrlManager
    {
        public static string FullSiteUrl()
        {
            string fullSiteUrl = String.Format("{0}{1}", UrlManager.GetSitePrefix(), ConfigManager.GetSiteBaseUrl());
            return fullSiteUrl;

        }

        public static string GetSitePrefix()
        {
            bool enforceSSL = Convert.ToBoolean(ConfigManager.EnforceSSL().Trim());
            return enforceSSL ? "https://" : "http://";
        }
    }
}
