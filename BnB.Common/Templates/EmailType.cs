﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB.Common.Templates
{
    internal class EmailType
    {
        //Admin Emails
        public const string AccountRegistered = "AccountRegistered.html";
        public const string WelcomeResent = "WelcomeResent.html";

        //Public Emails
        public const string Welcome = "WelcomeReceived.html";
        public const string AccountApproved = "WelcomeApproved.html";

        //Global Emails
        public const string AccountRecovery = "AccountRecovery.html";
        public const string PaymentReceipt = "Receipt.html";
    }
}
