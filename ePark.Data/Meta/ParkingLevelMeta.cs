using System;
using System.Collections.Generic;
    
namespace ePark.Data
{
    public class ParkingLevelMeta
    {
        public int ParkingLevelID { get; set; }
        public Nullable<int> VenueID { get; set; }
        public string ParkingLevelDesc { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ParkingGarage ParkingGarage { get; set; }
        public virtual ICollection<ParkingUnit> ParkingUnits { get; set; }
    }
}
