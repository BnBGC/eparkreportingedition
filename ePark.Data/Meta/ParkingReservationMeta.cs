﻿using System;

namespace ePark.Data
{
    public class ParkingReservationMeta
    {
        public int ParkingReservationID { get; set; }
        public int AspNetUsersID { get; set; }
        public Nullable<int> ParkingUnitID { get; set; }
        public Nullable<int> ReceiptID { get; set; }
        public Nullable<int> ParkingContractRatesID { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string ReservedMonth { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ParkingContractRate ParkingContractRate { get; set; }
        public virtual Receipt Receipt { get; set; }
    }
}
