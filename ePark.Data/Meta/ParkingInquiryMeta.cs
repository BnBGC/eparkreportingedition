using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ePark.Data
{
    public class ParkingInquiryMeta
    {
        public int ParkingInquiryID { get; set; }
        public Nullable<int> ASPNetUserId { get; set; }

        [DefaultValue(17)]
        public Nullable<int> ParkingGroupID { get; set; }
        public string ReservedMonth { get; set; }

        [DisplayName("Requested Start Date")]
        public Nullable<System.DateTime> ReservedMonthDateTime { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        [DisplayName("Parker First Name")]
        public string ParkerFirstName { get; set; }

        [DisplayName("Parker Last Name")]
        public string ParkerLastName { get; set; }
        public string Notes { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public Nullable<System.DateTime> ApprovedDate { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ParkingGroup ParkingGroup { get; set; }
    }
}
