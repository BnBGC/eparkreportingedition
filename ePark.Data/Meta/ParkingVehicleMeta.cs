using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ePark.Data
{   
    public class ParkingVehicleMeta
    {
        public int ParkingVehicleID { get; set; }
        public Nullable<int> AspNetUserID { get; set; }

        [DisplayName("Vehicle Make")]
        public string ParkingVehicleMake { get; set; }

        [DisplayName("Vehicle Model")]
        public string ParkingVehicleModel { get; set; }

        [DisplayName("Vehicle Year")]
        public string ParkingVehicleYear { get; set; }

        [DisplayName("Vehicle Color")]
        public string ParkingVehicleColor { get; set; }

        [DisplayName("Vehicle License")]
        public string ParkingVehicleLicense { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
