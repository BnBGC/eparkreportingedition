using System;
using System.Collections.Generic;
    
namespace ePark.Data
{
    public class EmailTemplateMeta
    {
        public int EmailTemplateID { get; set; }
        public string EmailShortDesc { get; set; }
        public string EmailDesc { get; set; }
        public string EmailText { get; set; }
    
        public virtual ICollection<EmailTran> EmailTrans { get; set; }
    }
}
