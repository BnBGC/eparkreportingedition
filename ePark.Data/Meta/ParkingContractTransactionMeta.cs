using System;

namespace ePark.Data
{    
    public class ParkingContractTransactionMeta
    {
        public int ParkingContractTransactionID { get; set; }
        public Nullable<int> ParkingContractTransactionTypeID { get; set; }
        public int ParkingContractID { get; set; }
        public Nullable<int> ParkingUnitID { get; set; }
        public Nullable<int> ReceiptID { get; set; }
        public Nullable<int> ParkingContractRatesID { get; set; }
        public Nullable<bool> Paid { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string ReservedMonth { get; set; }
        public Nullable<System.DateTime> ReservedMonthDateTime { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    
        public virtual ParkingContract ParkingContract { get; set; }
        public virtual ParkingContractRate ParkingContractRate { get; set; }
        public virtual ParkingContractTransactionType ParkingContractTransactionType { get; set; }
        public virtual Receipt Receipt { get; set; }
    }
}
