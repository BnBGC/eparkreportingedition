using System;
using System.Collections.Generic;
    
namespace ePark.Data
{
    public class EmailTranMeta
    {
        public int EmailTransID { get; set; }
        public Nullable<int> EmailTemplateID { get; set; }
        public Nullable<int> ParkingContractReservationID { get; set; }
        public Nullable<int> EmailStatusID { get; set; }
        public Nullable<System.Guid> AspNetUsersID { get; set; }
        public Nullable<int> transLogID { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string Comments { get; set; }
        public Nullable<System.DateTime> WillSendDate { get; set; }
        public Nullable<System.DateTime> SentDate { get; set; }
    
        public virtual EmailStatu EmailStatu { get; set; }
        public virtual EmailTemplate EmailTemplate { get; set; }
    }
}
