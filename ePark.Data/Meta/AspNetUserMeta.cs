using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ePark.Data
{
    public class AspNetUserMeta
    {
        public int Id { get; set; }
        public Nullable<int> AspNetUserProfile_Id { get; set; }

        [DisplayName("Confirm Email Address")]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }

        [DisplayName("Email Address")]
        public string UserName { get; set; }
    
        public virtual ICollection<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual AspNetUserProfile AspNetUserProfile { get; set; }
        public virtual ICollection<AspNetUsersContractXref> AspNetUsersContractXrefs { get; set; }
        public virtual ICollection<ParkingContract> ParkingContracts { get; set; }
        public virtual ICollection<ParkingInquiry> ParkingInquiries { get; set; }
        public virtual ICollection<ParkingVehicle> ParkingVehicles { get; set; }
        public virtual ICollection<AspNetRole> AspNetRoles { get; set; }
    }
}
