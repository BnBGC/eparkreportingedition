using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class SystemParameterMeta
    {
        public int ParametersID { get; set; }
        public Nullable<int> ParmValue { get; set; }
        public Nullable<decimal> ParmMoney { get; set; }
        public Nullable<decimal> ParmRate { get; set; }
        public string ParmDesc { get; set; }
    }
}
