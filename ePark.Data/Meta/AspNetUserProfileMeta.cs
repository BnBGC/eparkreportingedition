using System.Collections.Generic;
using System.ComponentModel;

namespace ePark.Data
{
    public class AspNetUserProfileMeta
    {
        public int Id { get; set; }

        [DisplayName("Billing First Name")]
        public string FirstName { get; set; }

        [DisplayName("Billing Last Name")]
        public string LastName { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int PostalCode { get; set; }

        [DisplayName("Primary Phone")]
        public string PhonePrimary { get; set; }
        public string PhoneSecondary { get; set; }
        public bool MccEmployee { get; set; }
        public bool CityEmployee { get; set; }

        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
    }
}
