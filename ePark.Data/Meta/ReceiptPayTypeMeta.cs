    using System;
    using System.Collections.Generic;

namespace ePark.Data
{
    public class ReceiptPayTypeMeta
    {
        public int ReceiptPayTypeID { get; set; }
        public string ReceiptPayTypeDesc { get; set; }
    
        public virtual ICollection<Receipt> Receipts { get; set; }
    }
}
