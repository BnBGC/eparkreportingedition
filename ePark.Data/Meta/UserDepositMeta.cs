﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePark.Data
{
    public class UserDepositMeta
    {
        public int UserDepositsID { get; set; }
        public Nullable<int> AspNetUsersID { get; set; }
        public Nullable<int> ReceiptID { get; set; }
        public Nullable<int> ParkingReservationID { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
        public virtual Receipt Receipt { get; set; }
    }
}
