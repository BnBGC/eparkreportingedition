namespace ePark.Data
{   
    public class AspNetUserClaimMeta
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
