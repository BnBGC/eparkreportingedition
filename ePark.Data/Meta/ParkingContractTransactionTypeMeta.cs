using System;
using System.Collections.Generic;

namespace ePark.Data
{   
    public class ParkingContractTransactionTypeMeta
    {
        public int ParkingContractTransactionTypeID { get; set; }
        public string ParkingTransactionDesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<ParkingContractTransaction> ParkingContractTransactions { get; set; }
    }
}
