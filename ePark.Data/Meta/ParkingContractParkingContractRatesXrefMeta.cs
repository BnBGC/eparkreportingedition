using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingContractParkingContractRatesXrefMeta
    {
        public int ParkingContractParkingContractRatesXrefID { get; set; }
        public Nullable<int> ParkingContractID { get; set; }
        public Nullable<int> ParkingContractRatesID { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> DateRangeBegin { get; set; }
        public Nullable<System.DateTime> DateRangeEnd { get; set; }
        public Nullable<bool> IsCurrent { get; set; }
    }
}
