using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ePark.Data
{
    public class BillingCardTypeMeta
    {   
        public int BillingCardTypesID { get; set; }

        [Required(ErrorMessage = "Card Description is required")]
        [DisplayName("Card Description")]
        public string CardDesc { get; set; }

        [Required(ErrorMessage = "Short Code to Identity Card Type")]
        [DisplayName("Card Code")]
        public string CardCode { get; set; }

        [DisplayName("Receipts List")]
        public virtual ICollection<Receipt> Receipts { get; set; }
    }
}
