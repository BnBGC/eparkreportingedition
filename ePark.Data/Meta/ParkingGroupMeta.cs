using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingGroupMeta
    {   
        public int ParkingGroupID { get; set; }
        public Nullable<int> ParkingGroupCategoryID { get; set; }
        public string GroupDesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<ParkingContract> ParkingContracts { get; set; }
        public virtual ParkingGroupCategory ParkingGroupCategory { get; set; }
        public virtual ICollection<ParkingGroupRate> ParkingGroupRates { get; set; }
        public virtual ICollection<ParkingInquiry> ParkingInquiries { get; set; }
    }
}
