using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ReceiptPayStatuMeta
    {
        public int ReceiptPayStatusID { get; set; }
        public string ReceiptPayStatusDesc { get; set; }
    
        public virtual ICollection<Receipt> Receipts { get; set; }
    }
}
