namespace ePark.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccessItem")]
    public partial class AccessItem
    {
        public int AccessItemID { get; set; }

        public int? ContractID { get; set; }

        [StringLength(50)]
        public string ItemNumber { get; set; }

        public DateTime? ActiveDate { get; set; }

        public DateTime? DeActiveDate { get; set; }

        public bool? IsActive { get; set; }

        public virtual Contract Contract { get; set; }
    }
}
