﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ePark.Data
{
    [MetadataType(typeof(AspNetRoleMeta))] public partial class AspNetRole {}
    [MetadataType(typeof(AspNetUserMeta))] public partial class AspNetUser {}
    [MetadataType(typeof(AspNetUserClaimMeta))] public partial class AspNetUserClaim {}
    [MetadataType(typeof(AspNetUserLoginMeta))] public partial class AspNetUserLogin {}
    [MetadataType(typeof(AspNetUserProfileMeta))] public partial class AspNetUserProfile {}
    //[MetadataType(typeof(AspNetUserContractXrefMeta))] public partial class AspNetUserContractXref {}
    [MetadataType(typeof(BillingCardTypeMeta))] public partial class BillingCardType {}
    [MetadataType(typeof(EmailStatuMeta))] public partial class EmailStatu {}
    [MetadataType(typeof(EmailTemplateMeta))] public partial class EmailTemplate {}
    [MetadataType(typeof(EmailTranMeta))] public partial class EmailTran {}
    [MetadataType(typeof(ParkingContractMeta))] public partial class ParkingContract {}
    [MetadataType(typeof(ParkingContractParkingContractRatesXrefMeta))] public partial class ParkingContractParkingContractRatesXref {}
    [MetadataType(typeof(ParkingContractRateMeta))] public partial class ParkingContractRate {}
    [MetadataType(typeof(ParkingContractStatuMeta))] public partial class ParkingContractStatu {}
    [MetadataType(typeof(ParkingEntityMeta))] public partial class ParkingEntity {}
    [MetadataType(typeof(ParkingGarageMeta))] public partial class ParkingGarage {}
    [MetadataType(typeof(ParkingGroupMeta))] public partial class ParkingGroup {}
    [MetadataType(typeof(ParkingGroupCategoryMeta))] public partial class ParkingGroupCategory {}
    [MetadataType(typeof(ParkingGroupRateMeta))] public partial class ParkingGroupRate {}
    [MetadataType(typeof(ParkingInquiryMeta))] public partial class ParkingInquiry {}
    [MetadataType(typeof(ParkingLevelMeta))] public partial class ParkingLevel {}
    [MetadataType(typeof(ParkingReservationMeta))] public partial class ParkingReservation {}
    [MetadataType(typeof(ParkingUnitMeta))] public partial class ParkingUnit {}
    [MetadataType(typeof(ParkingVehicleMeta))] public partial class ParkingVehicle {}
    [MetadataType(typeof(ParkingTransactionLogMeta))] public partial class ParkingTransactionLog {}
    [MetadataType(typeof(ReceiptMeta))] public partial class Receipt {}
    [MetadataType(typeof(ReceiptPayStatuMeta))] public partial class ReceiptPayStatu {}
    [MetadataType(typeof(ReceiptPayTypeMeta))] public partial class ReceiptPayType {}
    [MetadataType(typeof(SystemParameterMeta))] public partial class SystemParameter {}
    [MetadataType(typeof(UserDepositMeta))] public partial class UserDeposit {}

}
