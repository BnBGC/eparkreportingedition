﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.WebUI.Controllers;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EmailTemplatesController : BaseController
    {
        private EparkContext db = new EparkContext();

        // GET: Admin/EmailTemplates
        public async Task<ActionResult> Index()
        {
            return View(await db.EmailTemplates.ToListAsync());
        }

        // GET: Admin/EmailTemplates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailTemplate emailTemplate = await db.EmailTemplates.FindAsync(id);
            if (emailTemplate == null)
            {
                return HttpNotFound();
            }
            return View(emailTemplate);
        }

        // GET: Admin/EmailTemplates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/EmailTemplates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "EmailTemplateID,EmailShortDesc,EmailDesc,EmailText")] EmailTemplate emailTemplate)
        {
            if (ModelState.IsValid)
            {
                db.EmailTemplates.Add(emailTemplate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(emailTemplate);
        }

        // GET: Admin/EmailTemplates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailTemplate emailTemplate = await db.EmailTemplates.FindAsync(id);
            if (emailTemplate == null)
            {
                return HttpNotFound();
            }
            return View(emailTemplate);
        }

        // POST: Admin/EmailTemplates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "EmailTemplateID,EmailShortDesc,EmailDesc,EmailText")] EmailTemplate emailTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emailTemplate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(emailTemplate);
        }

        // GET: Admin/EmailTemplates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailTemplate emailTemplate = await db.EmailTemplates.FindAsync(id);
            if (emailTemplate == null)
            {
                return HttpNotFound();
            }
            return View(emailTemplate);
        }

        // POST: Admin/EmailTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            EmailTemplate emailTemplate = await db.EmailTemplates.FindAsync(id);
            db.EmailTemplates.Remove(emailTemplate);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
