﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.WebUI.Controllers;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SystemParametersController : BaseController
    {
        private EparkContext db = new EparkContext();

        // GET: Admin/SystemParameters
        public async Task<ActionResult> Index()
        {
            return View(await db.SystemParameters.ToListAsync());
        }

        // GET: Admin/SystemParameters/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            if (systemParameter == null)
            {
                return HttpNotFound();
            }
            return View(systemParameter);
        }

        // GET: Admin/SystemParameters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/SystemParameters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ParametersID,ParmValue,ParmMoney,ParmRate,ParmDesc")] SystemParameter systemParameter)
        {
            if (ModelState.IsValid)
            {
                db.SystemParameters.Add(systemParameter);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(systemParameter);
        }

        // GET: Admin/SystemParameters/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            if (systemParameter == null)
            {
                return HttpNotFound();
            }
            return View(systemParameter);
        }

        // POST: Admin/SystemParameters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ParametersID,ParmValue,ParmMoney,ParmRate,ParmDesc")] SystemParameter systemParameter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(systemParameter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(systemParameter);
        }

        // GET: Admin/SystemParameters/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            if (systemParameter == null)
            {
                return HttpNotFound();
            }
            return View(systemParameter);
        }

        // POST: Admin/SystemParameters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SystemParameter systemParameter = await db.SystemParameters.FindAsync(id);
            db.SystemParameters.Remove(systemParameter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
