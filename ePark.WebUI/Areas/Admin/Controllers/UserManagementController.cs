﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.WebUI.Areas.Admin.Models;
using ePark.Managers;
using ePark.WebUI.Controllers;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    public class UserManagementController : BaseController
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/UserManagement
        public ActionResult Index()
        {
            var userManagmentModel = new UserManagementModel();
            var vwAspNetUsers = new AspNetUserProfileRolesViewManager();

            userManagmentModel.Users = vwAspNetUsers.GetList();
            
            return View(userManagmentModel);
        }

        public JsonResult ProvideUsers()
        {
            return Json(GetUsers(), JsonRequestBehavior.AllowGet);
        }

        public Array GetUsers()
        {
            var viewAspNetUserProfileRolesManager = new AspNetUserProfileRolesViewManager();

            Array rows = viewAspNetUserProfileRolesManager.GetList().Select(x => new
            {
                x.AspNetUsersId,
                x.UserName,
                x.FirstName,
                x.LastName,
                x.Email,
                x.Address1,
                x.City,
                x.State,
                x.PhonePrimary,
                x.MccEmployee
            }).ToArray();

            return rows;
        }
        
        public ActionResult UserMaint(int userid)
        {
            var model = new UserManagementModel();
            var vwaspnetuser = new AspNetUserProfileRolesViewManager();
            model.Role = new AspNetRole();

            model.User = vwaspnetuser.GetByUserId(userid);

            //model.UserRole = new AspNetUserRole { UserId = (int)model.User.AspNetUsersId };

            //if (model.User.AspNetRolesId != null) model.UserRole.RoleId = (int) model.User.AspNetRolesId;

            if (model.User.Name != null) model.Role.Name = model.User.Name;

            return View(model);
        }

        [HttpPost]
        public ActionResult UserMaint(UserManagementModel model)
        {
            return RedirectToAction("Index");
        }

        private void UpdateUser(int userid)
        {


        }

    }
}
