﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.Areas.Admin.Models;
using ePark.WebUI.Controllers;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GroupsController : BaseController
    {
        private EparkContext db = new EparkContext();

        // GET: Admin/Groups
        public async Task<ActionResult> Index()
        {
            var model = new GroupsModel();
            var groupMgr = new GroupCategoryRateViewManager();
            model.GroupCatRateList = groupMgr.GetList();

            return View(model);
        }

        // GET: Admin/Groups/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // GET: Admin/Groups/Create
        public ActionResult Create()
        {
            ViewBag.GroupCategoryID = new SelectList(db.GroupCategories, "GroupCategoryID", "GroupCategoryDesc");
            return View();
        }

        // POST: Admin/Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GroupID,GroupCategoryID,GroupDesc,IsActive")] Group group)
        {
            if (ModelState.IsValid)
            {
                db.Groups.Add(group);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.GroupCategoryID = new SelectList(db.GroupCategories, "GroupCategoryID", "GroupCategoryDesc", group.GroupCategoryID);
            return View(group);
        }

        // GET: Admin/Groups/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            var model = new GroupsModel();
            var groupMgr = new GroupManager();
            var GroupRateMgr = new GroupCategoryRateManager();
            var grpRateMgr = new GroupRateManager();

            model.currGroup = new Group();
            model.currGroupRate = new GroupRate();
            
            model.currGroup = groupMgr.GetById((int)id);
            model.currGroupRate = grpRateMgr.GetByGroupId((int) id);
            return View(model);
        }

        // POST: Admin/Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(GroupsModel model)
        {
            if (ModelState.IsValid)
            {
                var Grpmgr = new GroupManager();
                var GroupRateMgr = new GroupRateManager();

                Grpmgr.Update(model.currGroup);
                GroupRateMgr.Update(model.currGroupRate);

                model.GroupCategories = GetGroupCategories();
                model.GroupCategoryID = 1;

                var groupMgr = new GroupCategoryRateViewManager();
                model.GroupCatRateList = groupMgr.GetList();

               // return RedirectToAction("Index");
            }

            return View("Index",model);
        }

        // GET: Admin/Groups/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Admin/Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Group group = await db.Groups.FindAsync(id);
            db.Groups.Remove(group);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private IEnumerable<SelectListItem> GetGroupCategories()
        {
            var grpCategoriesMgr = new GroupCategoryRateManager();

            var grpCategories = grpCategoriesMgr.GetList().Select(x =>
                                new SelectListItem
                                {
                                    Value = x.GroupCategoryID.ToString(),
                                    Text = x.GroupCategoryDesc
                                });

            return new SelectList(grpCategories, "Value", "Text");

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
