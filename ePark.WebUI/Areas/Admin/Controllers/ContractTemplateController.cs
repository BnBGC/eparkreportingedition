﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.Managers;
using BnB.Common.PDF4Net;
using ePark.WebUI.Controllers;
using O2S.Components.PDF4NET;
using O2S.Components.PDF4NET.Graphics;
using O2S.Components.PDF4NET.Graphics.Shapes;
using O2S.Components.PDF4NET.Security;
using O2S.Components.PDF4NET.PDFFile;
using O2S.Components.PDF4NET.Forms;
using O2S.Components.PDF4NET.Graphics.Shapes.Transparency;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ContractTemplateController : BaseController
    {

        PDFDocument PDFDoc = new PDFDocument();
        PDFDocument PDFDoc2 = new PDFDocument();
        PDFPage PDFPage = new PDFPage();

        private ContractDocument _document;

        string strPagePath = string.Empty;

        string strTemplatePath = string.Empty;

        string FieldName = string.Empty;

        private ContractDocument localDocument
        {

            get { return _document; }
            set { _document = value; }
        }
        // GET: Admin/ContractTemplate
        public ActionResult Index(int? userid)
        {
            var contractMgr = new ContractManager();

            var contract = contractMgr.GetByAspNetUserId((int)userid);

            if (contract != null)
            {
                if (contract.ContractID != null)
                {
                    PDFDocument DOC = new PDFDocument();
                    DOC = getDOC((int)contract.ContractID);

                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "inline:filename=artifact.pdf");
                    DOC.Save(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

            return RedirectToAction("Index", "UserManagement");
        }

        private PDFDocument getDOC(int contractid)
        {
            var DocMgr = new ContractDocumentManager();

            PDFFile.SerialNumber = "PDF4NET-M48P4-D5XBR-KXIFY-R0AK2-PUDN6";

            string sitebaseurl = ConfigurationManager.AppSettings["SiteBaseUrl"];

            localDocument = DocMgr.GetById(2);
            // localDocument = DocMgr.GetByFileName("MCC24Hour.pdf");
            strPagePath = localDocument.ContractFilePath.ToString() + localDocument.ContractFile.ToString();
            strTemplatePath = Server.MapPath(strPagePath);

            PDFDocument DOC = new PDFDocument(Server.MapPath(strPagePath));


            var engine = new TemplateEngine();
            var contractMgr = new ContractManager();
            var aspuserMgr = new AspNetUsersManager();
            var aspuserprofileMgr = new AspNetUserProfilesManager();
            var vehicleMgr = new VehicleManager();
            var ContractTransMgr = new ContractTransactionManager();
            var participantMgr = new ParticipantManager();
            var sysparmsMgr = new SystemParametersManager();
            var empMgr = new EmployerManager();

            var contract = contractMgr.GetById(contractid);

            var aspuser = aspuserMgr.GetById((int)contract.AspNetUserID);
            var aspuserprofile = aspuserprofileMgr.GetById((int)aspuser.AspNetUserProfile_Id);

            var vehicleList = vehicleMgr.GetByContract(contractid);

            var contracttransdeposit = ContractTransMgr.GetDepositforContract(contractid);
            var contracttranspayment = ContractTransMgr.GetPaymentforContract(contractid);

            var cbidfee = sysparmsMgr.CalcCBID(contracttranspayment.Amount);
            var salestax = sysparmsMgr.CalcTax(contracttranspayment.Amount);

            var participantlist = participantMgr.GetByContract(contractid);

            var emp = empMgr.GetByGetByAspNetUserId((int)contract.AspNetUserID);

            string addr = aspuserprofile.Address1 + " " + aspuserprofile.City + ", " + aspuserprofile.State + " " +
                          aspuserprofile.PostalCode;

            decimal totalamt = (decimal)contracttranspayment.Amount + cbidfee + salestax;
            totalamt = Math.Round(totalamt, 2, MidpointRounding.AwayFromZero);

            FieldName = "Customer";
            engine.writeFormField(DOC, FieldName, aspuserprofile.FirstName + " " + aspuserprofile.LastName);

            string parker = "";

            if (participantlist.Count > 0)
            {
                parker = participantlist.FirstOrDefault().FirstName + " " + participantlist.FirstOrDefault().LastName;
            }
            else
            {
                parker = aspuserprofile.FirstName + " " + aspuserprofile.LastName;
            }

            FieldName = "Parker";
            engine.writeFormField(DOC, FieldName, parker);


            FieldName = "BillingAddressCityStateZip";
            engine.writeFormField(DOC, FieldName, addr);

            FieldName = "Email";
            engine.writeFormField(DOC, FieldName, aspuser.Email);

            FieldName = "WPhone";
            engine.writeFormField(DOC, FieldName, aspuser.PhoneNumber);


            if (emp.Count > 0)
            {
                FieldName = "EmpNameStreetAddress";
                engine.writeFormField(DOC, FieldName, emp[0].BusinessName + " " + emp[0].Address1 + " " + emp[0].City);
            }


            if (vehicleList.Count > 0)
            {
                FieldName = "LPlate";
                engine.writeFormField(DOC, FieldName, vehicleList.FirstOrDefault().VehicleLicense);

                FieldName = "Make";
                engine.writeFormField(DOC, FieldName, vehicleList.FirstOrDefault().VehicleMake);

                FieldName = "Model";
                engine.writeFormField(DOC, FieldName, vehicleList.FirstOrDefault().VehicleModel);

                FieldName = "Color";
                engine.writeFormField(DOC, FieldName, vehicleList.FirstOrDefault().VehicleColor);
            }

            if (contracttransdeposit != null)
            {
                FieldName = "ActivationFee";
                engine.writeFormField(DOC, FieldName, ((int)contracttransdeposit.Amount).ToString("0.00"));
            }

            if (contracttranspayment != null)
            {
                FieldName = "Card";
                engine.writeFormField(DOC, FieldName, ((int)contracttranspayment.Amount).ToString("0.00"));

                FieldName = "SalesTax";
                engine.writeFormField(DOC, FieldName, salestax.ToString("0.00"));

                FieldName = "CBIDFee";
                engine.writeFormField(DOC, FieldName, cbidfee.ToString("0.00"));

                FieldName = "TotalDue";
                engine.writeFormField(DOC, FieldName, totalamt.ToString("0.00"));
            }


            return (DOC);

        }
    }
}