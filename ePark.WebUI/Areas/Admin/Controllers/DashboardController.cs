﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.WebUI.Areas.Admin.Models;
using ePark.Managers;
using ePark.WebUI.Areas.Admin.ViewModels;
using ePark.WebUI.Controllers;
using ePark.WebUI.Helpers;
using ePark.WebUI.Models;
using Microsoft.AspNet.Identity.Owin;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DashboardController : BaseController
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            var adminDashboardViewModel = new AdminDashboardViewModel();
            var inquiryManager = new InquiryManager();
            var inqContractTrans = new UserContractTransactionManager();
            var inqContractDocsMgr = new ContractDocumentManager();

            AspNetUserProfile aspnetUserProfile = null;
            if (Session["UserProfileModel"] != null)
            {
                aspnetUserProfile = (AspNetUserProfile)Session["UserProfileModel"];
            }

            adminDashboardViewModel.CurrentUserId = aspnetUserProfile.Id;

            //Fetch all the inquiries (both approved and not) withing the last x amount of days
            var allRecentInquiries = inquiryManager.GetViewList(120);  // Inquiries created within last 30 days.
            var ContractDocs = inqContractDocsMgr.GetList(); // all contract documents which contains contract id

            var unpaidContract = inqContractTrans.GetUnPaidContractTrans();// all unpaid contracts
            var paidContract = inqContractTrans.GetPaidContractTrans(); // all paid contracts

            List<int> unpaidids = unpaidContract.Select(x => x.AspNetUserId).Distinct().ToList();  // list of distinct aspnetuserids from unpaid contracts

            var distinctpaidcontractids = paidContract.Select(x => x.ContractID).Distinct().ToList();  // list of distinct contractid from paid contracts
            var distinctContractidswithDocs = ContractDocs.Select(x => x.ContractID).Distinct().ToList();
                        

            var nocontracts = distinctpaidcontractids.Where(x => !distinctContractidswithDocs.Contains(x)).ToList();
            // **********   Add Query for AspNetUserIds based on nocontracts   **********  //
           // var fullcontnodocs = paidContract.Where(x => nocontracts.Contains(x.ContractID)).ToList();
            var paidcontractsnodocscontractids = paidContract.Where(x => nocontracts.Contains(x.ContractID)).ToList();

            List<int> paidnodocaspnetids = paidcontractsnodocscontractids.Select(x => x.AspNetUserId).ToList();

            //var paidcontractsnodocsaspnetuserids = paidcontractsnodocscontractids.Where(x => x.AspNetUserId);

            //These are unapproved inquiries waiting to be evaluated
            ///adminDashboardViewModel.InquiriesAwaitingApproval = allRecentInquiries.Where(x => x.IsApproved == false).ToList();

            adminDashboardViewModel.InquiriesAwaitingApproval = allRecentInquiries.Where(x => x.IsApproved == false).ToList(); // 

            //Aging Inquiries means they were approved a while ago and still have not paid. These guys would be receiving some sort of email notice.
            adminDashboardViewModel.AgingInquiries = allRecentInquiries.Where(x => paidnodocaspnetids.Contains((int)x.Id)).ToList();
            //adminDashboardViewModel.AgingInquiries = allRecentInquiries.Where(x => x.IsApproved == true && x.ApprovedDate < DateTime.Today.AddDays(-10)).ToList();

            //The approved inquiries from the last 30 days where the person has not yet paid (or amount of days specified in above query)
            adminDashboardViewModel.InquiresApprovedPendingPayment = allRecentInquiries.Where(x => unpaidids.Contains(Convert.ToInt32(x.Id))).ToList();
                //allRecentInquiries.Where(x => x.IsApproved == true && x.ApprovedDate > DateTime.Today.AddDays(-10)).ToList();

            return View(adminDashboardViewModel);
        }

        public async Task<ActionResult> UpdateInquiries(int id)
        {
            var adminDashboardViewModel = new AdminDashboardViewModel();
            //vwParkingInquiryManager PIM = new vwParkingInquiryManager();
            //ParkingInquiryManager PIMgr = new ParkingInquiryManager();
            //ParkingInquiry PI = new ParkingInquiry();
            //PI = PIMgr.GetById(id);
            //PI.ApprovedDate = DateTime.Now;
            //PI.IsApproved = true;

            //PIMgr.Update(PI);

            //RI.RecentInquiries = PIM.GetRecent();
            //RI.ImmediateInquiries = PIM.GetImmediate();

            return View(adminDashboardViewModel);

            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

        }


    }
}
