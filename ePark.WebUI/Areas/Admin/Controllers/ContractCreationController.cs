﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.Areas.Admin.Models;
using BnB.Common.Mail;
using BnB.Common.PDF4Net;

using O2S.Components.PDF4NET;
using O2S.Components.PDF4NET.Graphics;
using O2S.Components.PDF4NET.Graphics.Shapes;
using O2S.Components.PDF4NET.Security;
using O2S.Components.PDF4NET.PDFFile;
using O2S.Components.PDF4NET.Forms;
using O2S.Components.PDF4NET.Graphics.Shapes.Transparency;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    public class ContractCreationController : Controller
    {
        #region Variables


        #endregion

        // GET: Admin/ContractCreation
        public ActionResult Index(int id)
        {
            var contractModel = popInquiry(id);

            return View(contractModel);
        }

        public ContractViewModel popInquiry(int id)
        {
            var contractModel = new ContractViewModel();
            var groupManager = new GroupManager();
            var inquiryViewMgr = new InquiryViewManager();
            var aspuser = new AspNetUsersManager();

            contractModel.InqView = inquiryViewMgr.GetByInqId(id);

            contractModel.UserName = contractModel.InqView.UserName;
            contractModel.InquiryID = contractModel.InqView.InquiryID;
            contractModel.ContractGroups = GetGroups();
            contractModel.Grp = groupManager.GetById(contractModel.InqView.GroupID.Value);
            contractModel.SelectedGroupId = contractModel.InqView.GroupID.Value;
            contractModel.IsApproved = (bool)contractModel.InqView.IsApproved;
            contractModel.InquiryCreateDate = contractModel.InqView.CreateDate;
            contractModel.Address1 = contractModel.InqView.Address1;
            contractModel.FirstLastName = contractModel.InqView.FirstName + " " + contractModel.InqView.LastName;
            contractModel.Email = contractModel.InqView.Email;
            contractModel.ASPNetUserId = contractModel.InqView.Id;
            contractModel.Amount = contractModel.InqView.Amount;
            return contractModel;
        }

        // GET: Admin/ContractCreation/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/ContractCreation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/ContractCreation/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/ContractCreation/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/ContractCreation/Edit/5
        [HttpPost]
        public ActionResult Edit(ContractViewModel model)
        {
            if (ModelState.IsValid)
            {
                ModelState.AddModelError("","You cannot leave Name blank");
                var userprofile = new AspNetUserProfilesManager();
                var aspnetusermgr = new AspNetUsersManager();
                var contractMgr = new ContractManager();

                var aspuser = aspnetusermgr.GetById((int) model.ASPNetUserId);
                var aspnetuserprofile = userprofile.GetById((int)model.ASPNetUserId);

                int grpselected = model.SelectedGroupId;
                model.GroupID = model.SelectedGroupId;
                int inqID = model.InquiryID;


                try
                {
                    // TODO: Add update logic here

                    UpdateInquiry(model);

                    if (model.IsApproved == true)
                    {

                        MailManager.EmailActivationInstructions((int)model.ASPNetUserId, aspuser.SecurityStamp);

                        model.ContractID = (int)contractMgr.GetByAspNetUserId(aspuser.Id).ContractID;

                        CreateContractTrans(model);

                    }

                }
                catch(Exception ex)
                {
                    string strMsg = ex.Message.ToString();
                    return View();
                }
                return View(model);
                return RedirectToAction("Index", new { id = inqID });
            }
            return View(model);
            return RedirectToAction("Index");
        }

        // GET: Admin/ContractCreation/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/ContractCreation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private IEnumerable<SelectListItem> GetGroups()
        {
            var grouprates = new GroupCategoryRateManager();

            var grplist = grouprates.GetList().Where(gr => gr.GroupRateIsActive == true && gr.GroupRateBegin <= System.DateTime.Now && System.DateTime.Now <= gr.GroupRateEnd)
                .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.GroupID.ToString(),
                                    Text = x.GroupDesc + " $" + string.Format("{0:#.00}", x.Amount)
                                });

            return new SelectList(grplist, "Value", "Text");

        }

        private void UpdateInquiry(ContractViewModel model)
        {
            var InqMgr = new InquiryManager();
            
            var Inq = InqMgr.GetById(model.InquiryID);
           

            Inq.GroupID = model.SelectedGroupId;
            Inq.IsApproved = model.IsApproved;
            Inq.ApprovedDate = System.DateTime.Now;

            int result = InqMgr.Update(Inq);

          
        }

        private void CreateContractTrans(ContractViewModel model)
        {
            var transmgr = new ContractTransactionManager();
            var groupcategoryMgr = new GroupCategoryRateManager();
            var sysparmsMGR = new SystemParametersManager();
            var sysparms = new SystemParameter();



            var contracttrans = newContractTrans(model);
            var groupcategory = groupcategoryMgr.GetByGroupId(model.GroupID);

            try
            {

                contracttrans.Amount = groupcategory.Amount;
                contracttrans.GroupID = model.GroupID;

                transmgr.Create(contracttrans);

                contracttrans.Amount = sysparmsMGR.GetByDesc("Deposit").ParmMoney;
                contracttrans.ContractTransactionTypeID = 2;

                transmgr.Create(contracttrans);

            }
            catch (Exception ex)
            {
                string strMsg = ex.Message.ToString();
                throw;
            }

           

        }

       
        private ContractTransaction newContractTrans(ContractViewModel model)
        {
            var contracttrans = new ContractTransaction();

            contracttrans.ContractTransactionTypeID = 1;
            contracttrans.CreatedDate = System.DateTime.Now;
            contracttrans.ContractID = model.ContractID;
            contracttrans.Amount =model.Amount;
            contracttrans.ReservedMonthDateTime = System.DateTime.Now;
            contracttrans.ReservedMonth = System.DateTime.Now.Month.ToString() + "/" +
                                          System.DateTime.Now.Year.ToString();
            contracttrans.Paid = false;
            
            return contracttrans;


        }
    }
}
