﻿using System.Web.Mvc;
using ePark.WebUI.Areas.Admin.Models;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    public class ReportingController : Controller
    {
        // GET: Reporting/Reporting
        public ActionResult Index(int id)
        {
            int reportid = id;
            var model = new ReportsModel();

            model.ReportID = id;
           // return RedirectToAction("RedirectToAspx", "Reporting", new { ReportID = id });
            return View();
        }


        public ActionResult InquiriesByDate(int id)
        {
            int reportid = id;
            var model = new ReportsModel();

            model.ReportID = id;
            // return RedirectToAction("RedirectToAspx", "Reporting", new { ReportID = id });
            return View();
        }

        public RedirectResult RedirectToAspx(int ReportID)
        {
            string returnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);

            return Redirect("../Reports/ViewReports.aspx");//"?id=" + ReportID.ToString());
        }

    }
}