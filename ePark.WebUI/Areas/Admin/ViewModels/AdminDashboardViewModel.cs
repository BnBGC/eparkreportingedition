﻿using System.Collections.Generic;
using ePark.Data;

namespace ePark.WebUI.Areas.Admin.ViewModels
{
    public class AdminDashboardViewModel
    {
        public int CurrentUserId { get; set; }
        public IList<vwInquiry> InquiriesAwaitingApproval { get; set; } // Unapproved inquiries
        public IList<vwInquiry> AgingInquiries { get; set; } // Approved already but the person still has not paid (past their selected start parking date)
        public IList<vwInquiry> InquiresApprovedPendingPayment { get; set; } // Approved recently, still within payment window
    }

}