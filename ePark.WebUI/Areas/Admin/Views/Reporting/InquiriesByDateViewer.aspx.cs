﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using ePark.WebUI.Areas.Admin.Reports;
using Telerik.Reporting;
using Telerik.ReportViewer.WebForms;

namespace ePark.WebUI.Areas.Admin.Views.Reporting
{


    public partial class InquiriesByDateViewer : System.Web.Mvc.ViewPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            litHeader.Text = "<span style='font-size: 16pt;'>Inquiries by Date</span>";

            //string parm = Request.QueryString["id"].ToString();
            var rptinstance = new Telerik.Reporting.InstanceReportSource();
            rptinstance.ReportDocument = new Reports.InquiriesByDate();
            ReportViewer1.ReportSource = rptinstance;
            ReportViewer1.Report = rptinstance.ReportDocument;
        }
    }
}