﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ViewReports.aspx.cs" Inherits="ePark.WebUI.Areas.Admin.Views.Reporting.ViewReports" %>
<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=9.1.15.731, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
</head>
<body>
<form id="form1" runat="server">
    <div style="height: 60px; overflow: auto;">
        <asp:literal runat="server" ID="litHeader"></asp:literal>
    </div>
    <div style="align-content: center; height: 800px; overflow: auto;">
        <telerik:ReportViewer ID="ReportViewer1" runat="server" Width="95%" Height="100%" skin="WebBlue">
        </telerik:ReportViewer>
    </div>
</form>
</body>
</html>