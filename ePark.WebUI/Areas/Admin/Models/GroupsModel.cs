﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ePark.Data;
using ePark.Managers;

namespace ePark.WebUI.Areas.Admin.Models
{
    public class GroupsModel
    {
        int GroupId { get; set; }


        public int GroupCategoryID { get; set; }

        public IEnumerable<SelectListItem> GroupCategories { get; set; }
        
        public List<vwGroupCategoryRate> GroupCatRateList { get; set; }

        public vwGroupCategoryRate vwgroupCatRate { get; set; }

        public Group currGroup { get; set; }

        public GroupRate currGroupRate { get; set; }

        public GroupCategory currGroupCategory { get; set; }
    }
}