﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePark.Data;

namespace ePark.WebUI.Areas.Admin.Models
{
    public class UserManagementModel
    {
        public int userId { get; set; }

        public List<vwAspNetUserProfileRole> Users { get; set; }

        public AspNetUser aspNetUsers { get; set; }

        public AspNetUserProfile aspnetuserprofile { get; set; }

        public vwAspNetUserProfileRole User { get; set; }

        //public AspNetUserRole UserRole { get; set; }

        public AspNetRole Role { get; set; }


    }
} 