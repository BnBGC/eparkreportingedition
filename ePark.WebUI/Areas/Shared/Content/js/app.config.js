	$.root_ = $('body');	
	$.navAsAjax = false; 
	$.sound_path = "sound/";
	$.sound_on = false; 
	var root = this,
	    debugState = false,
	    debugStyle = 'font-weight: bold; color: #00f;',
	    debugStyle_green = 'font-weight: bold; font-style:italic; color: #46C246;',
	    debugStyle_red = 'font-weight: bold; color: #ed1c24;',
	    debugStyle_warning = 'background-color:yellow',
	    debugStyle_success = 'background-color:green; font-weight:bold; color:#fff;',
	    debugStyle_error = 'background-color:#ed1c24; font-weight:bold; color:#fff;',
	    throttle_delay = 350,
	    menu_speed = 235,
	    menu_accordion = true,
	    enablebActiveWidgets = false,
	    localStoragebActiveWidgets = false,
	    sortablebActiveWidgets = true,
	    enableMobileWidgets = false,
	    fastClick = false;