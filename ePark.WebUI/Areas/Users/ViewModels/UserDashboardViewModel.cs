﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.WebUI.Areas.Users.ViewModels
{
    public class UserDashboardViewModel
    {
        public int CurrentUserId { get; set; }
        public IList<vwInquiry> InquiriesAwaitingPayment { get; set; } // needs to pay for
        public IList<vwInquiry> InquiriesActive { get; set; } // alreayd paid for
        public IList<vwInquiry> InquiriesPast { get; set; } // paid for and expired
    }
}
