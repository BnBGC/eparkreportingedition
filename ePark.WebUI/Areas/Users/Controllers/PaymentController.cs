﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.App_Helpers;
using ePark.WebUI.Areas.Users.Models;
using ePark.WebUI.Controllers;
using ePark.WebUI.Models;
using ePark.WebUI.FISPayment;
using ePark.WebUI.Models.Enums;
using Microsoft.Ajax.Utilities;

namespace ePark.WebUI.Areas.Users.Controllers
{
    public class PaymentController : BaseController
    {
        public decimal cbidfee { get; set; }
        public decimal tax { get; set; }

        public decimal amt { get; set; }

        public decimal ttlamt { get; set; }

        public string localids { get; set; }



        // GET: Payment
        public ActionResult Index(int? id)
        {
            var contractM = new ContractManager();
            var VehM = new VehicleManager();
            var PM = new PayModel();
            var fispayment = new FISPaymentHelper();
            var AppHelpers = new App_Helpers.Helpers();

            if (id != null)
            {
                PM.userid = (int)id;
                PM.userContract = contractM.GetByAspNetUserId((int)id);
                if (PM.userContract.AspNetUserID != null)
                    PM.listVehicles = VehM.GetByContract((int)PM.userContract.AspNetUserID);
                if (PM.userContract.AspNetUserID != null)
                    PM.vehiclelist = VehM.GetByContract((int)PM.userContract.AspNetUserID);
            }

            var vwPayments = new ReservationPaymentViewManager();

            if (id != null) PM.ResPaymentViewList = vwPayments.GetUnPaidForUserContractTrans((int)id);

            // Pay Types CC, Check, Cashiers Check......
            PM.PayTypes = fispayment.GetPayTypes();
            PM.SelectedPayTypeId = 6;

            PM.BillingCardTypes = GetBillingCardTypes();
            PM.BillingCardTypesID = 1;

            // PM.CCYearsItems = loadddlCCExpYear();
            PM.SelectedCCYearId = 1;

            var currPayment = PaymentPrep(PM.ResPaymentViewList);

            PM.ttlAmount = ttlamt;
            PM.ttlCBIDFee = cbidfee;
            PM.ttlTax = tax;
            PM.amt = amt;
            if (id != null) PM.ContractTranstctionIds = AppHelpers.buildUnPaidContractTransIds((int)id);


            //todo Ask Rex why he is returning payment instead of index here...
            return View("Payment", PM);


        }


        // GET: Admin/PaymentManagement/Receipt
        public ActionResult Receipt(string ids)
        {

            var receiptmodel = new ReceiptModel();
            var AspNetuser = new AspNetUsersManager();
            var AspNetuserprofile = new AspNetUserProfilesManager();
            var ResPaymentVWMgr = new ReservationPaymentViewManager();
            var contractMgr = new ContractManager();
            var receiptMgr = new ReceiptManager();

            receiptmodel.Currentdate = System.DateTime.Now.ToShortDateString();
            var currentTrans = selectedPayments(ids);

            if (ids != null)
            {
                var currContract = contractMgr.GetById(currentTrans.FirstOrDefault().ContractID);
                receiptmodel.aspNetUsers = AspNetuser.GetById((int)currContract.AspNetUserID);
                receiptmodel.aspnetuserprofile = AspNetuserprofile.GetById((int)receiptmodel.aspNetUsers.AspNetUserProfile_Id);

                receiptmodel.currContract = currContract;

                receiptmodel.CityStateZip = receiptmodel.aspnetuserprofile.Address1 + " " +
                                            receiptmodel.aspnetuserprofile.City + ", " +
                                            receiptmodel.aspnetuserprofile.State;

                receiptmodel.ResPayments = ReceiptPayments(ids);   //ResPaymentVWMgr.GetUnPaidForUserContractTrans((int)userid);

                receiptmodel.ttlAmount = ttlamt;
                receiptmodel.PaymentType = receiptmodel.ResPayments[0].ReceiptPayTypeDesc;

            }

            return View("Receipt", receiptmodel);
        }

        public JsonResult ProvidePayments(PayModel model)
        {
            return Json(GetPayments(model), JsonRequestBehavior.AllowGet);
        }

        public Array GetPayments(PayModel model)
        {
            var usercontracts = new UserContractTransactionManager();
            var sysParmsForCalc = new SystemParametersManager();

            Array rows = usercontracts.GetUnPaidContractTrans().Select(x => new
            {
                x.ContractTransactionID,
                x.UserName,
                x.FirstName,
                x.LastName,
                x.TransactionDesc,
                x.Amount
                //,CalcCbid = sysParmsForCalc.CalcCBID(x.Amount),
                //CalcTax = sysParmsForCalc.CalcTax(x.Amount),
                //ttlAmount = sysParmsForCalc.CalcCBID(x.Amount) + sysParmsForCalc.CalcTax(x.Amount) + x.Amount
            }).ToArray();

            return rows;
        }



        //public JsonResult TransEdit(int id, string oper, decimal amount)
        //{

        //    switch (oper)
        //    {
        //        case "edit":
        //            saveContractTrans(id, amount);
        //            break;
        //        default:
        //            break;
        //    }

        //    string newoper = oper;
        //    decimal newamount = amount;

        //    return Json(GetPayments(), JsonRequestBehavior.AllowGet);

        //}


        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> PaymentMaint(PayModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        foreach (vwReservationPayment contractTrans in currentPayments(model.ContractTranstctionIds))
        //        {

        //            if (model.CCard.Length > 10)
        //            {
        //                FISPayment.ApiService client = new FISPayment.ApiService();

        //                var request = new FISPayment.ProcessHeartbeatRequest();
        //                request.MerchantCode = "50BNA-NSVCC-MCCWF-G";

        //                var response = client.ProcessHeartbeat(request);

        //                var submitpayment = new FISPayment.SubmitPaymentRequest();

        //                int contractresult = saveContract(model, contractTrans);
        //            }
        //        }

        //        return View("Receipt", model);
        //    }

        //    int cc = saveTransLog(model);

        //    return View(model);
        //}


        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Pay(PayModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        foreach (vwReservationPayment contractTrans in currentPayments(model.ContractTranstctionIds))
        //        {

        //            if (model.CCard.Length > 10)
        //            {
        //                FISPayment.ApiService client = new FISPayment.ApiService();

        //                var request = new FISPayment.ProcessHeartbeatRequest();
        //                request.MerchantCode = "50BNA-NSVCC-MCCWF-G";

        //                var response = client.ProcessHeartbeat(request);

        //                var submitpayment = new FISPayment.SubmitPaymentRequest();

        //                int contractresult = saveContract(model, contractTrans);
        //            }
        //        }

        //        return View("Receipt", model);
        //    }

        //    int cc = saveTransLog(model);

        //    return View(model);
        //}


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Pay(PayModel model)
        {

            var vwPayments = new ReservationPaymentViewManager();
            vwPayments.GetUnPaidForUserContractTrans(model.userid);
            var ReceiptMgr = new ReceiptManager();
            model.receipt = new Receipt();
            var parmMgr = new SystemParametersManager();
            model.ResPaymentViewList = vwPayments.GetUnPaidForUserContractTrans(model.userid);

            TempData["ResViewList"] = model.ResPaymentViewList;

            model.receipt.ReceiptPayTypeID = model.SelectedPayTypeId;

            model.receipt.ReceiptID = saveReceipt(model);

            var errors = ModelState.Values.SelectMany(v => v.Errors);
            //if (ModelState.IsValid)
            //{
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;

            foreach (vwReservationPayment contractTrans in model.ResPaymentViewList)
            {

                contractTrans.Tax = 0;
                contractTrans.CBIDFee = 0;
                contractTrans.ConvFee = 0;
                if (!contractTrans.IsTaxExempt && contractTrans.ContractTransactionTypeID == 1)
                {
                    contractTrans.Tax = parmMgr.CalcTax(contractTrans.Amount);
                    tax = (decimal)(tax + contractTrans.Tax);
                }

                if (contractTrans.ContractTransactionTypeID == 1)
                {
                    contractTrans.CBIDFee = parmMgr.CalcCBID(contractTrans.Amount);
                    cbidfee = (decimal)(cbidfee + contractTrans.CBIDFee);
                }

                amt = (decimal)(amt + contractTrans.Amount);
                contractTrans.ConvFee = contractTrans.Amount + contractTrans.Tax + contractTrans.CBIDFee;
                int contractresult = saveContract(model, contractTrans);


                ttlamt = amt + tax + cbidfee;
            }
            TempData["ttlamount"] = ttlamt;

            var pmttrans = new PaymentTransactionLog();
            model.receipt.BillingCardTypesID = null;
            model.receipt.ReceiptID = saveReceipt(model);

            TempData["ReceiptID"] = model.receipt.ReceiptID;

            int cc = saveTransLog(model);


            // }
            TempData["PaymentProcessed"] = true;
            return RedirectToAction("Receipt", "Payment");

        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PayCC(PayModel model)
        {
            var vwPayments = new ReservationPaymentViewManager();
            vwPayments.GetUnPaidForUserContractTrans(model.userid);
            var ReceiptMgr = new ReceiptManager();
            model.receipt = new Receipt();
            var parmMgr = new SystemParametersManager();
            var fispayhelper = new FISPaymentHelper();

            FISPayment.ApiService client = new FISPayment.ApiService();
            var paymentRequest = new FISPayment.SubmitPaymentRequest();
            model.ResPaymentViewList = vwPayments.GetUnPaidForUserContractTrans(model.userid);

            TempData["ResViewList"] = model.ResPaymentViewList;

            if (ModelState.IsValid)
            {
                model.receipt.ReceiptID = saveReceipt(model);

                TempData["ttlamount"] = ttlamt;

                var AccessRequest = fispayhelper.AssesCardFees(model);

                try
                {
                    var assessfees = client.AssessFeesForCard(AccessRequest);
                    paymentRequest.TokenId = assessfees.TokenId;
                    paymentRequest.PaymentChargeTypeCode = "P";
                    paymentRequest.MerchantCode = "50BNA-NSVCC-MCCWF-G";


                    var SubmitResponse = client.SubmitPayment(paymentRequest);


                    string strMsg = SubmitResponse.Message.ToString();
                    string PaymentID = SubmitResponse.PaymentId;
                    string[] errmsg1 = SubmitResponse.ErrorMessages;

                    var pmttrans = new PaymentTransactionLog();

                    var paytrans = SubmitResponse.Transactions;


                    if (paytrans[0].RCString != "APPROVED")
                    {
                        CreateAlert(AlertType.Error, "Invalid Payment : " + paytrans[0].RCString);

                        int cc = saveTransLog(model, SubmitResponse);
                    }
                    else
                    {
                        model.receipt.ReceiptID = saveReceipt(model);

                        int cc = saveTransLog(model, SubmitResponse);

                        TempData["ReceiptID"] = model.receipt.ReceiptID;
                        
                        int ret = saveContractTran(model);

                        return RedirectToAction("Receipt", "Payment", new { ids = model.ContractTranstctionIds });
                    }

                   // CreateAlert(AlertType.Success, "Thank you for making your payment.");
                }
                catch (Exception ex)
                {
                    CreateAlert(AlertType.Error, ex.Message);
                }

                // only returning here because the Index View of Payments redirects to Payments... I need to return this error message on this view but will do this temporarily.
                return RedirectToAction("Index", "Dashboard", new { area = "Users" });
            }

            TempData["PaymentProcessed"] = true;
            return RedirectToAction("Receipt", "Payment");
        }

        private int saveContractTran(PayModel model)
        {
            var parmMgr = new SystemParametersManager();

            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            int cntr = 0;

            foreach (vwReservationPayment contractTrans in model.ResPaymentViewList)
            {
                if (model.CCard.Length > 10)
                {
                    cntr++;
                    contractTrans.Tax = 0;
                    contractTrans.CBIDFee = 0;
                    contractTrans.ConvFee = 0;
                    if (!contractTrans.IsTaxExempt && contractTrans.ContractTransactionTypeID == 1)
                    {
                        contractTrans.Tax = parmMgr.CalcTax(contractTrans.Amount);
                        tax = (decimal)(tax + contractTrans.Tax);
                    }

                    if (contractTrans.ContractTransactionTypeID == 1)
                    {
                        contractTrans.CBIDFee = parmMgr.CalcCBID(contractTrans.Amount);
                        cbidfee = (decimal)(cbidfee + contractTrans.CBIDFee);
                    }

                    amt = (decimal)(amt + contractTrans.Amount);
                    contractTrans.ConvFee = contractTrans.Amount + contractTrans.Tax + contractTrans.CBIDFee;

                    // Save Contract Transactions Receipts Receipt Xref
                    int contractresult = saveContract(model, contractTrans);

                }

                ttlamt = amt + tax + cbidfee;
            }

            return cntr;
        }

        private List<vwReservationPayment> currentPayments(string contractTransList)
        {
            var regPayMgr = new ReservationPaymentViewManager();
            var openPayments = regPayMgr.GetOpenContractTransList();

            openPayments = openPayments.Where(p => contractTransList.Contains(p.ContractTransactionID.ToString())).ToList();

            return openPayments;
        }


        private List<vwReservationPayment> selectedPayments(string contractTransList)
        {
            var regPayMgr = new ReservationPaymentViewManager();
            var selectedPayments = regPayMgr.GetList();

            selectedPayments = selectedPayments.Where(p => contractTransList.Contains(p.ContractTransactionID.ToString())).ToList();

            return selectedPayments;
        }

        private IEnumerable<SelectListItem> GetBillingCardTypes()
        {
            var syspaytypes = new ReceiptManager();

            var billingcardtypes = syspaytypes.GetBillingCardType().Select(x =>
                                new SelectListItem
                                {
                                    Value = x.BillingCardTypesID.ToString(),
                                    Text = x.CardDesc
                                });

            return new SelectList(billingcardtypes, "Value", "Text");

        }


        private IEnumerable<SelectListItem> loadddlCCExpYear()
        {

            var selectList =
    new SelectList(Enumerable.Range(System.DateTime.Now.Year, 12));

            return new SelectList(selectList, "Value", "Text");
        }

        private int saveTransLog(PayModel model, SubmitPaymentResponse payresp)
        {
            var translog = new PaymentTransactionLog();
            var translogMgr = new PaymentTransactionsLogManager();
            var recepitMgr = new ReceiptManager();
            var parmMgr = new SystemParametersManager();
            var resptrans = new SubmitPaymentResponse().Transactions;
            var contractMgr = new ContractManager();


            resptrans = payresp.Transactions;
            translog.transDate = System.DateTime.Now;

            model.receipt.ReceiptPayTypeID = 5;

            // transLogID
            string paytype = recepitMgr.GetPaymentType(model.receipt);
            translog.transQuery = paytype;

            BillingCardType billingCardType = recepitMgr.GetBillingCardType().FirstOrDefault(x => x.BillingCardTypesID == model.BillingCardTypesID);
            if (billingCardType !=
                null)
                translog.transCardType = billingCardType.CardDesc;

            translog.transRequestType = "PROD";
            translog.transMerchantCode = "50BNA-EBIDS-EBIDS-G";
            translog.transSettleCode = "50BNA-EBIDS-EBIDS-00";
            translog.transLast5Digits = Convert.ToInt32(model.CCard.Substring((model.CCard.Length - 5), 5));
            translog.transAmount = Convert.ToDouble(model.ttlAmount);
            // translog.transConvFee = parmMgr.CalcCBID(contractTrans.Amount);
            translog.transExpMM = model.CCMonthSelectedId;
            translog.transExpYY = model.SelectedCCYearId;
            translog.transCardSecurityValue = model.CVV;
            translog.transBillAddressSent = true;
            //transDataSent  FROM EPP
            translog.transReturnCode = resptrans[0].RC;
            translog.transResponseString = resptrans[0].RCString;
            translog.transTransactionID = resptrans[0].TransID.ToString();
            translog.transDateStamp = System.DateTime.Now;
            //transAuthorization
            translog.transStatus = resptrans[0].Message;
            translog.transUser1 = parmMgr.CalcCBID(model.ttlCBIDFee).ToString();
            translog.transUser2 = parmMgr.CalcTax(model.ttlTax).ToString();
            translog.transUser3 = contractMgr.GetByAspNetUserId(model.userid).ContractID.ToString();
            translog.UserId = model.userid;
            //translog.transUser3 = 
            //transQuery
            translog.TransReceiptID = model.receipt.ReceiptID;

            translogMgr.Create(translog);

            return 0;
        }


        private int saveTransLog(PayModel model)
        {
            var translog = new PaymentTransactionLog();
            var translogMgr = new PaymentTransactionsLogManager();
            var recepitMgr = new ReceiptManager();
            var parmMgr = new SystemParametersManager();
            var resptrans = new SubmitPaymentResponse().Transactions;
            // resptrans = payresp.Transactions;
            translog.transDate = System.DateTime.Now;
            
            // transLogID
            string paytype = recepitMgr.GetPaymentType(model.receipt);
            
            translog.transCardType = paytype;
            translog.transQuery = paytype;
            translog.transRequestType = "PROD";
            translog.transMerchantCode = "50BNA-EBIDS-EBIDS-G";
            translog.transSettleCode = "50BNA-EBIDS-EBIDS-00";
            if (model.CCard != null)
                translog.transLast5Digits = Convert.ToInt32(model.CCard.Substring((model.CCard.Length - 5), 5));
            translog.transAmount = Convert.ToDouble(model.ttlAmount);
            // translog.transConvFee = parmMgr.CalcCBID(contractTrans.Amount);
            translog.transExpMM = model.CCMonthSelectedId;
            translog.transExpYY = model.SelectedCCYearId;
            translog.transCardSecurityValue = model.CVV;
            translog.transBillAddressSent = true;
            //transDataSent  FROM EPP

            if (resptrans != null)
            {
                translog.transReturnCode = resptrans[0].RC;
                translog.transStatus = resptrans[0].Message;
                translog.transResponseString = resptrans[0].RCString;
                translog.transTransactionID = resptrans[0].TransID.ToString();
            }

            translog.transDateStamp = System.DateTime.Now;
            translog.transUser1 = parmMgr.CalcCBID(model.ttlCBIDFee).ToString();
            translog.transUser2 = parmMgr.CalcTax(model.ttlTax).ToString();
            translog.transUser3 = model.Contracttransaction.ContractID.ToString();
            translog.UserId = model.userid;
            translog.TransReceiptID = model.receipt.ReceiptID;

            translogMgr.Create(translog);

            return 0;
        }


        private int saveContract(PayModel model, vwReservationPayment contractTrans)
        {
            var contracttransmgr = new ContractTransactionManager();
            var contract = new ContractTransaction();
            var ContractreceiptXrefMgr = new ContractTransactionReceiptXrefManager();

            contract.ContractTransactionID = (int)contractTrans.ContractTransactionID;
            contract.ContractID = (int)contractTrans.ContractID;
            contract.ContractTransactionTypeID = contractTrans.ContractTransactionTypeID;
            contract.Paid = true;
            contract.Amount = contractTrans.Amount;
            contract.ReservedMonth = contractTrans.ReservedMonth;
            contract.ReservedMonthDateTime = contractTrans.ReservedMonthDateTime;
            contract.CreatedDate = contractTrans.CreatedDate;
            contract.GroupID = contractTrans.GroupID;

            saveTransReceiptXref(model, contractTrans);

            return contracttransmgr.Update(contract);

        }

        private int saveReceipt(PayModel model)
        {
            var receiptMgr = new ReceiptManager();
            var Receipt = new Receipt();

            Receipt.BillingCardTypesID = model.BillingCardTypesID;
            Receipt.ReceiptPayTypeID = model.SelectedPayTypeId;
            Receipt.ReceiptTotalAmount = model.ttlAmount;
            Receipt.CBIDFee = model.ttlCBIDFee;
            Receipt.Tax = model.ttlTax;
            Receipt.PayDate = System.DateTime.Now;
            Receipt.ReceiptPayStatusID = 2;

            return receiptMgr.Create(Receipt);

        }

        private void saveTransReceiptXref(PayModel model, vwReservationPayment trans)
        {
            var ContractreceiptXrefMgr = new ContractTransactionReceiptXrefManager();
            var ContractTransactionReceiptXref = new ContractTransactionReceiptXref();

            ContractTransactionReceiptXref.ReceiptID = model.receipt.ReceiptID;
            ContractTransactionReceiptXref.ContractTransactionID = trans.ContractTransactionID;
            ContractTransactionReceiptXref.CreateDate = System.DateTime.Now;
            ContractTransactionReceiptXref.IsActive = true;

            ContractreceiptXrefMgr.Create(ContractTransactionReceiptXref);
        }

        private void saveContractTrans(int TransID, decimal amount)
        {
            var contracttransmgr = new ContractTransactionManager();
            var existingContractTransaction = contracttransmgr.GetById(TransID);

            existingContractTransaction.Amount = amount;

            contracttransmgr.Update(existingContractTransaction);

        }


        protected List<vwReservationPayment> PaymentPrep(List<vwReservationPayment> pymnts)
        {
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            List<vwReservationPayment> currPayments = new List<vwReservationPayment>();
            var regPayMgr = new ReservationPaymentViewManager();
            var parmMgr = new SystemParametersManager();
            //var openPayments = regPayMgr.GetUnPaidContractTrans();
            var openPayments = regPayMgr.GetList();


            foreach (var trans in pymnts)
            {
                if (!trans.IsTaxExempt && trans.ContractTransactionTypeID == 1)
                {

                    trans.Tax = parmMgr.CalcTax(trans.Amount);
                    tax = (decimal)(tax + trans.Tax);
                }

                if (trans.ContractTransactionTypeID == 1)
                {
                    trans.CBIDFee = parmMgr.CalcCBID(trans.Amount);
                    cbidfee = (decimal)(cbidfee + trans.CBIDFee);
                }

                amt = (decimal)(amt + trans.Amount);

            }

            ttlamt = ttlamt + amt + tax + cbidfee;


            return pymnts;
        }

        protected List<vwReservationPayment> ReceiptPayments(string ids)
        {
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            List<vwReservationPayment> openPayments = new List<vwReservationPayment>();
            var regPayMgr = new ReservationPaymentViewManager();
            var parmMgr = new SystemParametersManager();
            openPayments = regPayMgr.GetPaidForUserContractTrans();

            openPayments = openPayments.Where(p => ids.Contains(p.ContractTransactionID.ToString())).ToList();

            // openPayments = regPayMgr.GetUnPaidForUserContractTrans(ids);
            if (TempData != null)
            {

                foreach (var trans in openPayments)
                {
                    trans.Tax = 0;
                    trans.CBIDFee = 0;
                    trans.ConvFee = 0;
                    if (!trans.IsTaxExempt && trans.ContractTransactionTypeID == 1)
                    {
                        trans.Tax = parmMgr.CalcTax(trans.Amount);
                        tax = (decimal)(tax + trans.Tax);
                    }

                    if (trans.ContractTransactionTypeID == 1)
                    {
                        trans.CBIDFee = parmMgr.CalcCBID(trans.Amount);
                        cbidfee = (decimal)(cbidfee + trans.CBIDFee);
                    }

                    amt = (decimal)(amt + trans.Amount);
                    trans.ConvFee = trans.Amount + trans.Tax + trans.CBIDFee;

                }

                ttlamt = ttlamt + amt + tax + cbidfee;
            }
            return openPayments;
        }


    }
}