﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.Areas.Users.ViewModels;
using ePark.WebUI.Controllers;
using ePark.WebUI.Models;
using ePark.WebUI.Models.Enums;
using ePark.WebUI.ViewModels;
using Microsoft.AspNet.Identity;

namespace ePark.WebUI.Areas.Users.Controllers
{
    public class ParticipantsController : BaseController
    {

        private readonly EparkContext _db = new EparkContext();

        // GET: Participants
        public ActionResult Index(int? id)
        {
            var userId = User.Identity.GetUserId<int>();

            var participantsList = new List<Participant>();
            var participantManager = new ParticipantManager();
            var contractManager = new ContractManager();

            var contract = contractManager.GetByAspNetUserId(userId);

            if (contract != null)
            {
                participantsList = participantManager.GetByContract(contract.ContractID);
            }
            else
            {
                CreateAlert(AlertType.Error, "You do not have an active contract.");
            }

            return View(participantsList);
        }

        // GET: Participants/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participant participant = await _db.Participants.FindAsync(id);
            if (participant == null)
            {
                return HttpNotFound();
            }
            return View(participant);
        }

        // GET: Participants/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Participants/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirstName,LastName,Relation,IsActive")] Participant participant)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var contractmgr = new ContractManager();

                var contract = contractmgr.GetByAspNetUserId(userId);
                participant.ContractID = contract.ContractID;

                var participantManager = new ParticipantManager();
                participantManager.Create(participant);

                CreateAlert(AlertType.Success, "You successfully created a new participant");
            }
            else
            {
                CreateAlert(AlertType.Error, "An error was encountered attempting to save your particpant, please try again.");
                return View(participant);
            }

            return RedirectToAction("Index", "Participants", new { area = "Users", id = User.Identity.GetUserId() });
        }

        // GET: Participants/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var participantManager = new ParticipantManager();
            var model = participantManager.GetById(id.Value);

            return View("Edit", model);
        }

        // POST: Participants/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ParticipantID,ContractID,FirstName,LastName,Relation,IsActive")] Participant participant)
        {
            if (ModelState.IsValid)
            {
                var participantManager = new ParticipantManager();

                participantManager.Update(participant);
                CreateAlert(AlertType.Success, "You successfully created a new participant");
            }
            else
            {
                CreateAlert(AlertType.Error, "An error was encountered attempting to save your particpant, please try again.");
                return View(participant);
            }

            return RedirectToAction("Index", "Participants", new { area = "Users", id = User.Identity.GetUserId() });
        }

        // GET: Participants/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participant participant = await _db.Participants.FindAsync(id);
            if (participant == null)
            {
                return HttpNotFound();
            }
            return View(participant);
        }

        // POST: Participants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Participant participant = await _db.Participants.FindAsync(id);
            _db.Participants.Remove(participant);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
