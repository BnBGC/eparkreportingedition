﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.Models;
using ePark.WebUI.Helpers;
using ePark.WebUI.Controllers;
using Microsoft.AspNet.Identity;

namespace ePark.WebUI.Areas.Users.Controllers
{
    public class PaymentHistoryController : BaseController
    {
        // GET: PaymentHistory
        public ActionResult PaymentHist(int? id)
        {
            TempData["UserId"] = id.ToString();

            var PM = new PayModel();
            PM.userid = (int)id;
            var vwPayments = new ReservationPaymentViewManager();

            if (id != null) PM.ResPaymentViewList = vwPayments.GetUnPaidForUserContractTrans((int)id);


            return View(PM);
        }

        // GET: PaymentHistory/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //string sidx, string sord, int page, int rows, bool _search, string searchField, string searchOper, string searchString
        //string sidx, string sort, int page, int rows, string user, DateTime? paiddate)
        public JsonResult ProvidePayHistory(string sidx, string sord, int page, int rows, bool _search, string searchField, string searchOper, string searchString, PayModel pm)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var PM = new PayModel();
                        
            Array pymnts = GetPayments();

            if (_search)
            {
                 pymnts = GetSearchPayments(searchField, searchOper, searchString, pm);
            }
            
            int totalRecords = pymnts.Length;
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

            PM.userid = Convert.ToInt32(TempData["UserId"]);
            return Json(pymnts, JsonRequestBehavior.AllowGet);
        }
        
        public Array GetPayments()
        {
            var usercontracts = new UserContractTransactionManager();
            var sysParmsForCalc = new SystemParametersManager();
            


            Array rows = usercontracts.GetTrans().Select(x => new
            {
                x.AspNetUserId,
                x.ContractTransactionID,
                x.UserName,
                Name = x.FirstName.ToString() + " " + x.LastName.ToString(),
                x.GroupDesc,
                x.TransactionDesc,
                x.Amount
            }
                ).Where(x => x.AspNetUserId == Convert.ToInt32(TempData["UserId"])).ToArray();
            
           
            return rows;
        }

        public Array GetSearchPayments(string searchField, string searchOper, string searchString, PayModel pm)
        {
            var usercontracts = new UserContractTransactionManager();
            var sysParmsForCalc = new SystemParametersManager();
           
            var param = Expression.Parameter(typeof(vwUserContractTransaction), "p");

            MethodCallExpression dynamiclambda = GenDynamicLinq(searchField, searchOper, searchString, param);
            
            Expression<Func<vwUserContractTransaction, bool>> predicate = Expression.Lambda<Func<vwUserContractTransaction, bool>>(dynamiclambda, param);

            Func<vwUserContractTransaction, bool> compiled = predicate.Compile();

            Array rows = usercontracts.GetUnPaidContractTrans().Where(compiled).Select(x => new
            {
                x.AspNetUserId,
                x.ContractTransactionID,
                x.UserName,
                Name = x.FirstName.ToString() + " " + x.LastName.ToString(),
                x.GroupDesc,
                x.TransactionDesc,
                x.Amount
            }).Where(x => x.AspNetUserId == Convert.ToInt32(User.Identity.GetUserId())).ToArray();


            return rows;

        }

        public MethodCallExpression GenDynamicLinq(string searchField, string searchOper, string searchString, ParameterExpression param)
        {
            MethodCallExpression call = null;
            MethodInfo comparemethod = null;

            switch (searchOper)
            {
                case "eq":
                    comparemethod = typeof(String).GetMethod("Equals", new Type[] { typeof(String) }); // Type of Search eq, bw, 
                    break;

                case "bw":
                        comparemethod = typeof(String).GetMethod("StartsWith", new Type[] { typeof(String) }); // Type of Search eq, bw, 
                    break;

                case "ew":
                    comparemethod = typeof(String).GetMethod("EndsWith", new Type[] { typeof(String) }); // Type of Search eq, bw, 
                    break;

                case "cn":
                        comparemethod = typeof(String).GetMethod("Contains", new Type[] { typeof(String) }); // Type of Search eq, bw, 
                    break;

            }

            PropertyInfo property = typeof(vwUserContractTransaction).GetProperty(searchField);  // Name, UserName
            //var param = Expression.Parameter(typeof(vwUserContractTransaction), "p");

            MemberExpression propertyAccess = Expression.MakeMemberAccess(param, property); // "p"  Name
            call = Expression.Call(propertyAccess, comparemethod, Expression.Constant(searchString)); // Value to search for

            return call;
        }

    }
}
