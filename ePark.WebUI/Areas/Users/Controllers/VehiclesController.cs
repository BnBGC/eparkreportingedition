﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.Controllers;
using ePark.WebUI.Models;
using ePark.WebUI.Models.Enums;
using Microsoft.AspNet.Identity;

namespace ePark.WebUI.Areas.Users.Controllers
{
    public class VehiclesController : BaseController
    {
        private EparkContext db = new EparkContext();

        // GET: Vehicles
        public ActionResult Index(int? id)
        {
            var userId = User.Identity.GetUserId<int>();

            var vehicleList = new List<Vehicle>();
            var vehicleManager = new VehicleManager();
            var contractManager = new ContractManager();

            var contract = contractManager.GetByAspNetUserId(userId);

            if (contract != null)
            {
                vehicleList = vehicleManager.GetByContract(contract.ContractID);
            }
            else
            {
                CreateAlert(AlertType.Error, "You do not have an active contract.");
            }

            return View(vehicleList);
        }

        // GET: Vehicles/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = await db.Vehicles.FindAsync(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // GET: Vehicles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AspNetUserID,ContractID,VehicleMake,VehicleModel,VehicleYear,VehicleColor,VehicleLicense,IsActive")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var contractmgr = new ContractManager();

                var contract = contractmgr.GetByAspNetUserId(userId);
                vehicle.ContractID = contract.ContractID;

                var vehicleManager = new VehicleManager();
                vehicleManager.Create(vehicle);

                CreateAlert(AlertType.Success, "You successfully created a new vehicle");
            }
            else
            {
                CreateAlert(AlertType.Error, "An error was encountered attempting to save your vehicle, please try again.");
                return View(vehicle);
            }

            return RedirectToAction("Index", "Vehicles", new { area = "Users", id = User.Identity.GetUserId() });
        }

        // GET: Vehicles/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var vehicleManager = new VehicleManager();
            var model = vehicleManager.GetById(id.Value);

            return View("Edit", model);
        }

        // POST: Vehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VehicleID,AspNetUserID,ContractID,VehicleMake,VehicleModel,VehicleYear,VehicleColor,VehicleLicense,IsActive")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                var vehicleManager = new VehicleManager();

                vehicleManager.Update(vehicle);
                CreateAlert(AlertType.Success, "You successfully created a new vehicle");
            }
            else
            {
                CreateAlert(AlertType.Error, "An error was encountered attempting to save your vehicle, please try again.");
                return View(vehicle);
            }

            return RedirectToAction("Index", "Vehicles", new { area = "Users", id = User.Identity.GetUserId() });
        }

        // GET: Vehicles/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehicle vehicle = await db.Vehicles.FindAsync(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // POST: Vehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Vehicle vehicle = await db.Vehicles.FindAsync(id);
            db.Vehicles.Remove(vehicle);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
