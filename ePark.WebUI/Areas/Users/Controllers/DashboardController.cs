﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.WebUI.Areas.Admin.Models;
using ePark.Managers;
using ePark.WebUI.Areas.Admin.ViewModels;
using ePark.WebUI.Areas.Users.ViewModels;
using ePark.WebUI.Controllers;
using ePark.WebUI.Helpers;
using ePark.WebUI.Models;
using Microsoft.AspNet.Identity.Owin;

namespace ePark.WebUI.Areas.Users.Controllers
{
    [Authorize(Roles = "Public")]
    public class DashboardController : BaseController
    {
        // GET: Admin/Dashboard
        public ActionResult Index()
        {
            var userDashboardViewModel = new UserDashboardViewModel();
            var inquiryManager = new InquiryManager();

            //Fetch all the inquiries (both approved and not) withing the last x amount of days
            var allRecentInquiries = inquiryManager.GetViewList(30);

            AspNetUserProfile aspnetUserProfile = null;
            if (Session["UserProfileModel"] != null)
            {
                aspnetUserProfile = (AspNetUserProfile)Session["UserProfileModel"];
            }

            userDashboardViewModel.CurrentUserId = aspnetUserProfile.Id;

            //These are unapproved inquiries waiting to be evaluated
            userDashboardViewModel.InquiriesAwaitingPayment = allRecentInquiries.Where(x => x.AspNetUserProfilesId == userDashboardViewModel.CurrentUserId).ToList();

            return View(userDashboardViewModel);
        }
    }
}
