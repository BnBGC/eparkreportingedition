﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ePark.Data;
using ePark.Managers;

namespace ePark.WebUI.Areas.Users.Models
{
    public class ReceiptModel
    {
        public int userId { get; set; }

        public string Currentdate { get; set; }

        public string userName { get; set; }

        public string CityStateZip { get; set; }

        public List<vwAspNetUserProfileRole> Users { get; set; }

        public AspNetUser aspNetUsers { get; set; }

        public AspNetUserProfile aspnetuserprofile { get; set; }

        public vwAspNetUserProfileRole User { get; set; }

        public decimal ttlAmount { get; set; }

        public string PaymentType { get; set; }

        public Contract currContract { get; set; }

        public string ContractID { get; set; }

        public decimal ttlCBIDFee { get; set; }

        public decimal ttlTax { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}")]
        public decimal amt { get; set; }

        public int SelectedPayTypeId { get; set; }
        public IEnumerable<SelectListItem> PayTypes { get; set; }

        public int BillingCardTypesID { get; set; }
        public IEnumerable<SelectListItem> BillingCardTypes { get; set; }

        public int CCMonthSelectedId { get; set; }

        public List<vwReservationPayment> ResPayments { get; set; }

        public string PaymentMethod { get; set; }


    }
}