﻿using ePark.WebUI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ePark.Data;


namespace ePark.WebUI.ViewModels
{
    public class ParkingInquiryViewModel
    {
        public int SelectedParkingPeriodID { get; set; }
        public IEnumerable<SelectListItem> ParkingPeriods { get; set; }

        public int SelectedParkingGroupID { get; set; }
        public IEnumerable<SelectListItem> ParkingGroups { get; set; }

        public Inquiry Inquiry { get; set; } // use this

        public Employer Employer { get; set; }

        public Contract Contract { get; set; }

        public Participant Participant { get; set; }

        public Vehicle Vehicle { get; set; }  // this has to be List<ParkingVehicle>

    }
}