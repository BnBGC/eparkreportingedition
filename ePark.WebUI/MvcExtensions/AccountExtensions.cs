﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace ePark.WebUI.MvcExtensions
{
    public static class AccountExtensions
    {
            public static string FullName(this IPrincipal user)
            {
                if (user.Identity.IsAuthenticated)
                {
                    var claimsIdentity = user.Identity as ClaimsIdentity;
                    if (claimsIdentity != null)
                        foreach (var claim in claimsIdentity.Claims.Where(claim => claim.Type == ClaimTypes.GivenName))
                        {
                            return claim.Value;
                        }
                    return String.Empty;;
                }
                return String.Empty;
            }
    }
}