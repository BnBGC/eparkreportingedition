﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace ePark.WebUI.MvcExtensions
{
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// Renders checkbox as ont input (normal Html.CheckBoxFor renders two inputs: checkbox and hidden)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="html"></param>
        /// <param name="expression"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString BasicCheckBoxFor<T>(this HtmlHelper<T> html, Expression<Func<T, bool>> expression, object htmlAttributes = null)
        {
            var tag = new TagBuilder("input");

            tag.Attributes["type"] = "checkbox";
            tag.Attributes["id"] = html.IdFor(expression).ToString();
            tag.Attributes["name"] = html.NameFor(expression).ToString();
            tag.Attributes["value"] = "true";

            // set the "checked" attribute if true
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            if (metadata.Model != null)
            {
                bool modelChecked;
                if (Boolean.TryParse(metadata.Model.ToString(), out modelChecked))
                {
                    if (modelChecked)
                    {
                        tag.Attributes["checked"] = "checked";
                    }
                }
            }

            // merge custom attributes
            tag.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            var tagString = tag.ToString(TagRenderMode.SelfClosing);
            return MvcHtmlString.Create(tagString);
        }

        public static string IsActive(this HtmlHelper html, string control, string action)
        {
            var routeData = html.ViewContext.RouteData;
            var routeAction = (string)routeData.Values["action"];
            var routeControl = (string)routeData.Values["controller"];
            var returnActive = control == routeControl && action == routeAction;

            return returnActive ? "active" : "";
        }

    }
}