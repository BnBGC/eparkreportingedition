﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BnB.Common.Mail;
using ePark.Data;
using ePark.Managers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ePark.WebUI.Models;
using ePark.WebUI.Models.Enums;
using ePark.WebUI.ViewModels;
using AspNetUserProfile = ePark.WebUI.Models.AspNetUserProfile;

namespace ePark.WebUI.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult GetUserDetails()
        {
            AspNetUserProfile aspnetUserProfile = null;
            if (Session["UserProfileModel"] != null)
            {
                aspnetUserProfile = (AspNetUserProfile)Session["UserProfileModel"];
            }

            return aspnetUserProfile != null ? Content(new MvcHtmlString(String.Format("{0} {1}", aspnetUserProfile.FirstName, aspnetUserProfile.LastName)).ToHtmlString()) : null;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            //if (string.IsNullOrEmpty(returnUrl) && Request.UrlReferrer != null)
            //    returnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
            //ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            AspNetUsersManager user = new AspNetUsersManager();
            // var aspnetUser = user.GetByUserName(model.Email);

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);


            switch (result)
            {
                case SignInStatus.Success:

                    var applicationUser = UserManager.FindByEmail(model.Email);
                    var aspNetUserProfilesManager = new AspNetUserProfilesManager();

                    var aspNetUserProfile = aspNetUserProfilesManager.GetById(applicationUser.Id);

                    AspNetUserProfile userProfileModel = new AspNetUserProfile
                    {
                        Id = aspNetUserProfile.Id,
                        FirstName = aspNetUserProfile.FirstName,
                        LastName = aspNetUserProfile.LastName
                    };

                    Session["UserProfileModel"] = userProfileModel;

                    if (!UserManager.IsInRole(applicationUser.Id, "Admin"))
                    {
                        // return RedirectToAction("Index", "Payment", new { id = applicationUser.Id });

                        // if they have pending payments on an inquiry send them to it... otherwise to their user dashboard.
                        return RedirectToAction("Index", "Dashboard", new { area = "Users" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
                    }
                    return RedirectToLocal(returnUrl);
                //  return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut: return View("Lockout");
                case SignInStatus.RequiresVerification: return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:

                    CreateAlert(AlertType.Error, "The username / password combination you entered does not exist in our system.");
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);

            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/ParkingInquiry
        [AllowAnonymous]
        public ActionResult ParkingInquiry()
        {
            var parkingInquiryViewModel = new ParkingInquiryViewModel
            {
                ParkingGroups = GetParkingGroups(),
                ParkingPeriods = GetParkingPeriods()
            };

            return View(parkingInquiryViewModel);
        }

        //
        // POST: /Account/ParkingInquiry
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ParkingInquiry(ParkingInquiryViewModel model)
        {
            var existingUser = await UserManager.FindByEmailAsync(model.Inquiry.AspNetUser.Email.Trim());

            if (existingUser != null)
            {
                ModelState.AddModelError("", "Email address already exists.");
                CreateAlert(AlertType.Error, "The email address you selected is already in use. If you already have an account, log in to create a new parking inquiry.");
            }

            if (ModelState.IsValid)
            {
                if (model.SelectedParkingPeriodID == 0)
                {
                    model.SelectedParkingPeriodID = 1;
                }

                var user = new ApplicationUser
                {
                    UserName = model.Inquiry.AspNetUser.UserName,
                    Email = model.Inquiry.AspNetUser.UserName,
                    PhoneNumber = model.Inquiry.AspNetUser.AspNetUserProfile.PhonePrimary

                };

                var aspNetUserProfile = new AspNetUserProfile
                {
                    FirstName = model.Inquiry.AspNetUser.AspNetUserProfile.FirstName,
                    LastName = model.Inquiry.AspNetUser.AspNetUserProfile.LastName,
                    PhonePrimary = model.Inquiry.AspNetUser.AspNetUserProfile.PhonePrimary,
                    Address1 = model.Inquiry.AspNetUser.AspNetUserProfile.Address1,
                    Address2 = model.Inquiry.AspNetUser.AspNetUserProfile.Address2,
                    City = model.Inquiry.AspNetUser.AspNetUserProfile.City,
                    State = model.Inquiry.AspNetUser.AspNetUserProfile.State,
                    PostalCode = model.Inquiry.AspNetUser.AspNetUserProfile.PostalCode,
                    MccEmployee = false,
                    CityEmployee = false
                };

                user.AspNetUserProfile = aspNetUserProfile;

                var result = await UserManager.CreateAsync(user, String.Format("A1b2{0}", System.Web.Security.Membership.GeneratePassword(6, 2)));
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "Public");

                    //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    //await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    var inquiryManager = new InquiryManager();
                    var parkingVehicleManager = new VehicleManager();
                    var employerManager = new EmployerManager();

                    model.Contract = new Contract();
                    model.Participant = new Participant();

                    model.Inquiry.ASPNetUserId = user.Id;
                    model.Vehicle.AspNetUserID = user.Id;
                    model.Employer.AspNetUserID = user.Id;

                    inquiryManager.Create(model.Inquiry);

                    model.Contract.ContractID = createContract(model);

                    model.Vehicle.ContractID = model.Contract.ContractID;
                    model.Participant.ContractID = model.Contract.ContractID;

                    string strvehicle = model.Vehicle.VehicleMake + model.Vehicle.VehicleColor +
                                        model.Vehicle.VehicleLicense + model.Vehicle.VehicleModel;

                    string strparticipant = model.Inquiry.ParkerFirstName + model.Inquiry.ParkerLastName;

                    if (!String.IsNullOrWhiteSpace(model.Employer.BusinessName))
                    {
                        employerManager.Create(model.Employer);
                    }

                    if (strvehicle.Length > 0)
                    {
                        model.Vehicle.IsActive = true;
                        model.Vehicle.VehicleID = parkingVehicleManager.Create(model.Vehicle);
                    }

                    if (strparticipant.Length > 0)
                    {
                        model.Participant.IsActive = true;
                        model.Participant.ParticipantID = createParticipant(model);
                    }

                    MailManager.EmailWelcome(user.Id);

                    return RedirectToAction("Thanks", "Account");
                }
                AddErrors(result);
            }

            model.ParkingPeriods = GetParkingPeriods();
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Thanks()
        {
            return View();
        }

        private IEnumerable<SelectListItem> GetParkingGroups()
        {
            //var dbUserRoles = new vw();
            GroupCategoryRateManager groupCategoryRateManager = new GroupCategoryRateManager();

            var groups = groupCategoryRateManager.GetGroupByCategoryId(1).Select(x => new SelectListItem { Value = x.GroupID.ToString(), Text = x.GroupDesc + " / " + string.Format("{0:c}", x.Amount) });
            //
            return new SelectList(groups, "Value", "Text");
        }

        private IEnumerable<SelectListItem> GetParkingPeriods()
        {
            DateTime now = DateTime.Now;
            List<SelectListItem> items = new List<SelectListItem>();

            for (int i = 0; i < 6; i++)
            {
                //         Console.WriteLine(now.ToString("MMMM"));
                items.Add(new SelectListItem() { Text = string.Format("{0}", now.ToString("MMMM") + "/" + now.Year), Value = i.ToString(), Selected = false });
                now = now.AddMonths(1);

            }

            return new SelectList(items, "Value", "Text");
        }

        //// GET: /Account/ConfirmInquiry
        //[AllowAnonymous]
        //public async Task<ActionResult> AutoActivate(int userId)
        //{
        //    // Temporary for testing automatically activate the user as if Heidi did it herself... Code used on admin side to activate user
        //    var applicationUser = UserManager.FindById(userId);

        //    var aspNetUserProfilesManager = new AspNetUserProfilesManager();
        //    var aspNetUserProfile = aspNetUserProfilesManager.GetById(applicationUser.Id);
        //    MailManager.EmailActivationInstructions(userId, applicationUser.SecurityStamp);

        //    return View();
        //}


        //// GET: /Account/ConfirmInquiry
        //[AllowAnonymous]
        //public async Task<ActionResult> ExternalEmail(int userId, string Code)
        //{
        //    if (userId == null || Code == null)
        //    {
        //        return View("Error");
        //    }

        //    var emailConfirmationCode = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
        //    var result = await UserManager.ConfirmEmailAsync(userId, emailConfirmationCode);

        //    if (result.Succeeded)
        //    {
        //        string seccode = UserManager.GetSecurityStamp(userId);

        //        if (Code == seccode)
        //        {
        //            return RedirectToAction("Index", "Payment", new { id = userId });
        //        }
        //    }

        //    // return View(result.Succeeded ? "Confirmed" : "Error");
        //    return View("Login");
        //}


        // GET: /Account/ConfirmInquiry
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmInquiry(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Login");
            }

            var userIdProper = Convert.ToInt32(userId);
            var result = await UserManager.ConfirmEmailAsync(userIdProper, code);

            //if (result.Succeeded)
            //{
            return RedirectToAction("InquiryPasswordReset", "Account", new { userId = userId, code = code });
            //}
            //else
            //{
            //    return View("Login");
            //}
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);

            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {

                var user = await UserManager.FindByEmailAsync(model.Email);

                if (user != null) // || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    if (await UserManager.IsEmailConfirmedAsync(user.Id))
                    {
                        // Don't reveal that the user does not exist or is not confirmed
                        string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                        MailManager.EmailAccountRecovery(user.Id, callbackUrl);

                        return RedirectToAction("ForgotPasswordConfirmation", "Account");
                    }
                    else
                    {
                        ModelState.AddModelError("", "You cannot reset your password until your inquiry has been approved.");
                    }
                }


            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            //var usermgr = new AspNetUsersManager();
            //var pswdresetmdl = new ResetPasswordViewModel();

            //var aspuser = usermgr.GetBySECStamp(code);
            //pswdresetmdl.Email = aspuser.Email;
            //pswdresetmdl.Code = aspuser.SecurityStamp;

            return code == null ? View("Error") : View();
            //return View(pswdresetmdl);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public async Task<ActionResult> InquiryPasswordReset(string userId, string code)
        {
            var resetPasswordViewModel = new ResetPasswordViewModel();
            var applicationUser = await UserManager.FindByIdAsync(Convert.ToInt32(userId));

            resetPasswordViewModel.Email = applicationUser.Email;
            resetPasswordViewModel.Code = code;

            //return code == null ? View("Error") : View();
            return View(resetPasswordViewModel);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InquiryPasswordReset(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

            var result = await UserManager.ResetPasswordAsync(user.Id, code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Dashboard", new { area = "Users" });
            }
            AddErrors(result);
            return View();
        }


        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("ParkingInquiry", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                userId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string userId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (userId != null)
                {
                    properties.Dictionary[XsrfKey] = userId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        private int createContract(ParkingInquiryViewModel model)
        {
            var contractManager = new ContractManager();

            model.Contract = new Contract();

            model.Contract.AspNetUserID = model.Inquiry.ASPNetUserId;
            model.Contract.ContractStatusID = 1;
            model.Contract.IsActive = true;
            model.Contract.CreateDate = System.DateTime.Now;
            model.Contract.ContractID = contractManager.Create(model.Contract);

            return (int)model.Contract.ContractID;

        }


        private int createParticipant(ParkingInquiryViewModel model)
        {
            var participantMger = new ParticipantManager();

            model.Participant.FirstName = model.Inquiry.ParkerFirstName;
            model.Participant.LastName = model.Inquiry.ParkerLastName;
            model.Participant.ContractID = model.Contract.ContractID;
            model.Participant.IsActive = true;

            return participantMger.Create(model.Participant);

        }

        #endregion
    }
}