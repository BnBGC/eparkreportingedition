﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ePark.WebUI.ViewModels;
using ePark.Managers;

namespace ePark.WebUI.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Thanks()
        {
            return View("Index");
        }

        [HttpPost]
        public JsonResult SubmitParkingInquiry(ParkingInquiryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = "error" }
                };
            }

            //insert manager in now code instead with instance of service

            //instead return partial for parking inquiry thanks (no more not-redirect)
            //return new JsonResult()
            //{
            //    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
            //    Data = new { result = "success" }
            //};

            return Json(new
            {
                redirectUrl = Url.Action("Thanks", "Home"),
                isRedirect = true
            });
        }


    }
}