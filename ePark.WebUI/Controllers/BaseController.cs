﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.WebUI.Helpers;
using ePark.WebUI.Models.Enums;

namespace ePark.WebUI.Controllers
{
    public class BaseController : Controller
    {
        public void CreateAlert(AlertType alertType, string alertMessage)
        {
            TempData["Alert"] = AlertHelper.CreateAlert(alertType, alertMessage);
        }
    }
}