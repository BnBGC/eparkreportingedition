﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePark.Data;

namespace ePark.WebUI.Models
{
    public class VehicleViewModel
    {

        public List<Vehicle> vehicles { get; set; }

        public int AspNetUserId { get; set; }


    }
}