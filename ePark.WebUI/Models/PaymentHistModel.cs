﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ePark.Data;

namespace ePark.WebUI.Models
{
    public class PaymentHistModel
    {

        [DisplayName("User:   ")]
        public string User { get; set; }

        [DisplayName("Payment Date:   ")]
        public string PaymentDate { get; set; }
        
        public List<vwReservationPayment> ResPaymentViewList { get; set; }
    }
}