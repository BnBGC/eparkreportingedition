﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePark.Data;
using ePark.WebUI.Controllers;

namespace ePark.WebUI.Models
{
    public class PaymentModel
    {
        public int userid { get; set; }

        public AspNetUser currUser { get; set; }

        public Vehicle currVehicle { get; set; }

        public List<ContractTransaction> userContractTransList { get; set; }

        public List<Vehicle> listVehicles { get; set; }

        public Participant currParticipant { get; set; }

        public List<Participant> listParticipants { get; set; }


        public List<Pass> listPasses { get; set; }

        public Contract userContract { get; set; }

        public IEnumerable<Vehicle> vehiclelist { get; set; }

        //Payment.cshtml Page BELOW
        public List<vwReservationPayment> ResPaymentViewList { get; set; }
    }
}