﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePark.WebUI.Models.Enums
{
    public enum AlertType
    {
        Success,
        Info,
        Warning,
        Alert,
        Error
    }
}