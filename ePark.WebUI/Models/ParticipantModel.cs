﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePark.Data;
using ePark.WebUI.Controllers;

namespace ePark.WebUI.Models
{
    public class ParticipantModel
    {
        public int userid { get; set; }

        public AspNetUser currUser { get; set; }

       
        public Contract userContract { get; set; }

        public IEnumerable<Participant> participantlist { get; set; }

        //Payment.cshtml Page BELOW
        public List<vwReservationPayment> ResPaymentViewList { get; set; }

    }
}