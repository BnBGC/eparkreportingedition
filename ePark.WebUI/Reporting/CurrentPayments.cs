namespace ePark.WebUI.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for CurrentPayments.
    /// </summary>
    public partial class CurrentPayments : Telerik.Reporting.Report
    {
        public CurrentPayments()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private void CurrentPayments_NeedDataSource(object sender, EventArgs e)
        {
            var report = (Telerik.Reporting.Processing.Report)sender;


            dsPayments.Parameters["@BeginDate"].Value = ReportParameters["BeginDate"].Value;
            dsPayments.Parameters["@EndDate"].Value = ReportParameters["EndDate"].Value;

            report.DataSource = dsPayments;
        }
    }
}