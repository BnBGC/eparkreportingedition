﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.Areas.Admin.Controllers;
using ePark.WebUI.Models;
using ePark.WebUI.FISPayment;
using ePark.WebUI.App_Helpers;


namespace ePark.WebUI.App_Helpers
{
    public class FISPaymentHelper
    {

        public decimal cbidfee { get; set; }
        public decimal tax { get; set; }

        public decimal amt { get; set; }

        public decimal ttlamt { get; set; }

        public string localids { get; set; }

        public AssessFeesForCardRequest AssesCardFees(PayModel model)
        {
            FISPayment.ApiService client = new FISPayment.ApiService();
            var AspNetProfMgr = new AspNetUserProfilesManager();
            var AspUserMgr = new AspNetUsersManager();

            var request = new FISPayment.AssessFeesForCardRequest();

            var user = AspUserMgr.GetById(model.userid);
            var profile = AspNetProfMgr.GetById((int)user.AspNetUserProfile_Id);

            request.BillingInfo = new BillingInfo();
            request.LineItems = new LineItem[1] { new LineItem() };

            request.AccountNumber = model.CCard;
            request.Cvv = model.CVV;
            request.MerchantCode = "50BNA-NSVCC-MCCWF-G";
            request.MerchantPassword = "BBTStest50";
            request.SettleMerchantCode = "50BNA-NSVCC-MCCWF-G";
            request.ExpirationMonthSpecified = true;
            request.ExpirationMonth = 12;//Convert.ToInt32(model.CCExpMonth);
            request.ExpirationYearSpecified = true;
            request.ExpirationYear = 2017;//Convert.ToInt32(model.CCExpYear);
            request.BillingInfo.BillingName = profile.FirstName + " " + profile.LastName;
            request.BillingInfo.BillingAddress = profile.Address1;
            request.BillingInfo.BillingCity = profile.City;
            request.BillingInfo.BillingEmail = user.Email;
            request.BillingInfo.BillingPostalCode = profile.PostalCode.ToString();
            request.BillingInfo.BillingPhone = profile.PhonePrimary;
            request.BillingInfo.BillingState = "TN";
            request.LineItems[0].LineItemNumber = 1;
            request.LineItems[0].ItemAmount = Convert.ToDecimal(model.ttlAmount);
            request.LineItems[0].ItemQuantity = 1;
            request.LineItems[0].ItemAmountSpecified = true;
            request.LineItems[0].ItemQuantitySpecified = true;
            request.LineItems[0].SettleMerchantCode = "50BNA-NSVCC-MCCWF-00";
            request.LineItems[0].MerchantAmount =  Convert.ToDecimal(model.ttlAmount);
            request.LineItems[0].MerchantAmountSpecified = true;
            request.LineItems[0].UserPart1 = DateTime.Now.ToLongDateString();
            request.LineItems[0].UserPart2 = "Group Description";
            request.LineItems[0].UserPart3 = Convert.ToDecimal(model.ttlAmount).ToString();
            
            return request;

            // client.AssessFeesForCard(request);
            
        }
        

        public IEnumerable<SelectListItem> GetPayTypes()
        {
            var syspaytypes = new ReceiptManager();

            var paytypes = syspaytypes.GetPayType().Select(x =>
                                new SelectListItem
                                {
                                    Value = x.ReceiptPayTypeID.ToString(),
                                    Text = x.ReceiptPayTypeDesc
                                });

            return new SelectList(paytypes, "Value", "Text");

        }


        protected List<vwReservationPayment> PaymentPrep(string newids)
        {
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            List<vwReservationPayment> currPayments = new List<vwReservationPayment>();
            var regPayMgr = new ReservationPaymentViewManager();
            var parmMgr = new SystemParametersManager();
            //var openPayments = regPayMgr.GetUnPaidContractTrans();
            var openPayments = regPayMgr.GetList();

            //if (TempData != null)
            //{

                currPayments = openPayments.Where(p => newids.Contains(p.ContractTransactionID.ToString())).ToList();

                foreach (var trans in currPayments)
                {
                    if (!trans.IsTaxExempt && trans.ContractTransactionTypeID == 1)
                    {

                        trans.Tax = parmMgr.CalcTax(trans.Amount);
                        tax = (decimal)(tax + trans.Tax);
                    }

                    if (trans.ContractTransactionTypeID == 1)
                    {
                        trans.CBIDFee = parmMgr.CalcCBID(trans.Amount);
                        cbidfee = (decimal)(cbidfee + trans.CBIDFee);
                    }

                    amt = (decimal)(amt + trans.Amount);

                }

                ttlamt = ttlamt + amt + tax + cbidfee;
         //   }

            return currPayments;
        }

    }
}