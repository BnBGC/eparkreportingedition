﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ePark.Data;
using ePark.Managers;

namespace ePark.WebUI.App_Helpers
{
    public class Helpers
    {
        public string buildUnPaidContractTransIds(int UserId)
        {
            var vwPayments = new ReservationPaymentViewManager();
            var ResPaymentViewList = new List<vwReservationPayment>();

            string ids = string.Empty;

            if (UserId != null)
            {
                ResPaymentViewList = vwPayments.GetUnPaidForUserContractTrans((int) UserId).ToList();

                foreach (var pymnt in ResPaymentViewList)
                {
                    if (ids.Length > 0)
                        ids = ids + ",";
                    ids = ids + pymnt.ContractTransactionID.ToString();
                }

            }

            return ids;

        }

    }
}