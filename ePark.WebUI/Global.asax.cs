﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ePark.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //for web api async
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        //void Application_Error(object sender, EventArgs e)
        //{
        //    if (Context.IsCustomErrorEnabled)
        //    {
        //        Exception ex = Server.GetLastError();

        //        if (ex != null)
        //        {
        //            if (ex.Message == "File does not exist.")
        //                return;

        //            HttpContext currentcontext = HttpContext.Current;
        //            currentcontext.Cache.Remove("EBIDError");
        //            currentcontext.Cache["EBIDError"] = ex;
        //            Response.Redirect("~/PublicError.aspx");
        //        }
        //    }
        //}
    }
}
