﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Mvc;
using ePark.WebUI.Models.Enums;

namespace ePark.WebUI.Helpers
{
    public class AlertHelper
    {
        public static HtmlString GetAvailableAlerts(ViewContext viewContext)
        {
            return (MvcHtmlString) viewContext.TempData["Alert"];
        }

        public static MvcHtmlString CreateAlert(AlertType alertType, string alertMessage)
        {
            //string baseAlert =
            //    "<div class=\"alert alert-success fade in\"><button class=\"close\" data-dismiss=\"alert\">×</button>" +
            //    "<i class=\"fa - fw fa fa-check\"></i><strong>Success</strong> The page has been added.</div>";

            string alertClass = "alert-success";
            string fontIconClass = "fa-fw fa fa-check";
            string strongText = "Success";

            switch (alertType)
            {
                case AlertType.Success:
                    alertClass = "alert-success";
                    fontIconClass = "fa-fw fa fa-check";
                    strongText = "SUCCESS";
                    break;
                case AlertType.Info:
                    alertClass = "alert-info";
                    fontIconClass = "fa-fw fa fa-info";
                    strongText = "NOTICE";
                    break;
                case AlertType.Warning:
                    alertClass = "alert-warning";
                    fontIconClass = "fa-fw fa fa-warning";
                    strongText = "WARNING";
                    break;
                case AlertType.Alert:
                    alertClass = "alert-warning";
                    fontIconClass = "fa-fw fa fa-warning";
                    strongText = "ALERT";
                    break;
                case AlertType.Error:
                    alertClass = "alert-danger";
                    fontIconClass = "fa-fw fa fa-times";
                    strongText = "ERROR";
                    break;
                default:
                    //throw new ArgumentOutOfRangeException(nameof(alertType), alertType, null);
                    throw new ArgumentOutOfRangeException("", "", null);
            }

            return
                MvcHtmlString.Create(
                    String.Format(
                        "<div class=\"alert {0} fade in\"><button class=\"close\" data-dismiss=\"alert\">×</button><i class=\"{1}\"></i><strong>{2}</strong>&nbsp;&nbsp;{3}</div>",
                        alertClass, fontIconClass, strongText, alertMessage));

        }
    }
}