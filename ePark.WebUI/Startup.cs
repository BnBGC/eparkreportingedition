﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ePark.WebUI.Startup))]
namespace ePark.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
