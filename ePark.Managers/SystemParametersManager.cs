﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class SystemParametersManager
    {


        public SystemParameter GetByDesc(string desc)
        {
            // if not found will return null
            using (var sysparmService = new SystemParametersService())
            {
                return sysparmService.GetByDesc(desc);
            }
        }


        public int Create(SystemParameter sysParm)
        {
            using (var sysParmService = new SystemParametersService())
            {
                return sysParmService.Create(sysParm);
            }
        }

        public int Update(SystemParameter sysParm)
        {
            using (var sysParmService = new SystemParametersService())
            {
                return sysParmService.Update(sysParm);
            }
        }

        public decimal CalcTax(decimal? amt)
        {

            using (var sysParmService = new SystemParametersService())
            {
                return sysParmService.TaxCalc(amt);
            }
        }

        public decimal CalcCBID(decimal? amt)
        {

            using (var sysParmService = new SystemParametersService())
            {
                return sysParmService.CBIDCalc(amt);
            }
        }

    }
}
