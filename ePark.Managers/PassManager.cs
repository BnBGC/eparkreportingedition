﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class PassManager
    {
        public Pass GetById(int PassId)
        {
            using (var passService = new PassServices())
            {
                return passService.GetById(PassId);
            }
        }

        public List<Pass> GetAll()
        {
            using (var passService = new PassServices())
            {
                return passService.GetAll();
            }
        }
        
        public Pass GetByPassNo(string passNo)
        {
            using (var passService = new PassServices())
            {
                return passService.GetByNo(passNo);
            }
        }

        public List<Pass> GetByContract(int ContractId)
        {
            using (var passService = new PassServices())
            {
                return passService.GetByContractId(ContractId);
            }
        }

        public int Create(Pass pass)
        {
            using (var passService = new PassServices())
            {
                return passService.Create(pass);
            }
        }

        public int Update(Pass passId)
        {
            using (var passService = new PassServices())
            {
                return passService.Create(passId);
            }
        }
    
    }
}
