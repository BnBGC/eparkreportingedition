﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class GroupCategoryRateViewManager
    {

        public List<vwGroupCategoryRate> GetList()
        {
            using (var groupcategoryrateviewservices = new GroupCategoryRateService())
            {
                return groupcategoryrateviewservices.GetList();
            }
        }


        public vwGroupCategoryRate GetByGroupId(int groupId)
        {
            using (var groupcategoryrateviewservices = new GroupCategoryRateService())
            {
                return groupcategoryrateviewservices.GetByGroupId(groupId);
            }
        }

    }
}
