﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class PaymentTransactionsLogManager
    {

        public PaymentTransactionLog GetById(int transLogId)
        {
            using (var contractService = new PaymentTransactionLogsService())
            {
                return contractService.GetById(transLogId);
            }
        }

        public List<PaymentTransactionLog> GetList()
        {
            using (var paymentTransactionLogsService = new PaymentTransactionLogsService())
            {
                return paymentTransactionLogsService.GetList();
            }
        }
        public int Create(PaymentTransactionLog payTrans)
        {
            using (var paymentTransactionLogsService = new PaymentTransactionLogsService())
            {
                return paymentTransactionLogsService.Create(payTrans);
            }
        }

        public int Update(PaymentTransactionLog payTrans)
        {
            using (var paymentTransactionLogsService = new PaymentTransactionLogsService())
            {
                return paymentTransactionLogsService.Update(payTrans);
            }
        }


    }
}
