﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class AspNetUserProfilesManager
    {
        public AspNetUserProfile GetById(int aspNetUserProfileId)
        {
            using (var aspNetUserProfilesService = new AspNetUserProfilesService())
            {
                return aspNetUserProfilesService.GetById(aspNetUserProfileId);
            }
        }

        public List<AspNetUserProfile> GetList()
        {
            using (var aspNetUserProfilesService = new AspNetUserProfilesService())
            {
                return aspNetUserProfilesService.GetList();
            }
        }
        public int Create(AspNetUserProfile aspNetUserProfile)
        {
            using (var aspNetUserProfilesService = new AspNetUserProfilesService())
            {
                return aspNetUserProfilesService.Create(aspNetUserProfile);
            }
        }

        public int Update(AspNetUserProfile aspNetUserProfile)
        {
            using (var aspNetUserProfilesService = new AspNetUserProfilesService())
            {
                return aspNetUserProfilesService.Update(aspNetUserProfile);
            }
        }
    }
}
