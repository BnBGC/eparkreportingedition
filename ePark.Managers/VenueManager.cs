﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class VenueManager
    {
        public Venue GetById(int venuId)
        {
            using (var venueService = new VenueService())
            {
                return venueService.GetById(venuId);
            }
        }

        public List<Venue> GetList()
        {
            using (var venueService = new VenueService())
            {
                return venueService.GetList();
            }
        }
        public int Create(Venue venue)
        {
            using (var venueService = new VenueService())
            {
                return venueService.Create(venue);
            }
        }

        public int Update(Venue venue)
        {
            using (var venueService = new VenueService())
            {
                return venueService.Update(venue);
            }
        }

        public int Delete(Venue venue)
        {
            using (var venueService = new VenueService())
            {
                return venueService.Delete(venue);
            }
        }

        public int DeleteById(int venuId)
        {
            using (var venueService = new VenueService())
            {
                return venueService.DeleteById(venuId);
            }
        }
    }
}
