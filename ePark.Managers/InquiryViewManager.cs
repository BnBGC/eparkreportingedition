﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class InquiryViewManager
    {

        public vwInquiry GetByUserId(int userId)
        {
            using (var inquiryViewService = new InquiryViewService())
            {
                return inquiryViewService.GetByUserId(userId);
            }
        }

        public vwInquiry GetByInqId(int id)
        {
            using (var inquiryViewService = new InquiryViewService())
            {
                return inquiryViewService.GetByInqId(id);
            }
        }

        public List<vwInquiry> GetList()
        {
            using (var inquiryViewService = new InquiryViewService())
            {
                return inquiryViewService.GetList();
            }
        }

        public List<vwInquiry> GetRecent()
        {
            using (var inquiryViewService = new InquiryViewService())
            {
                return inquiryViewService.GetRecentInquiries(14);
            }
        }

        public List<vwInquiry> GetImmediate()
        {
            using (var inquiryViewService = new InquiryViewService())
            {
                return inquiryViewService.GetImmediateInquiries(14);
            }
        }
    }
}
