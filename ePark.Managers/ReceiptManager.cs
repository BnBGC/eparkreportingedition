﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class ReceiptManager
    {
        public Receipt GetById(int receiptId)
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.GetById(receiptId);
            }
        }

        public List<Receipt> GetList()
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.GetList();
            }
        }

        public List<ReceiptPayType> GetPayType()
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.GetPayTypeList();
            }
        }

        public string GetPaymentType(Receipt receipt)
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.getPaymentMtehod(receipt);
            }
        }

        public List<BillingCardType> GetBillingCardType()
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.GetBillingCardList();
            }
        }

        public BillingCardType GetBillingCardTypeById(int TypeID)
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.GetBillingCardTypeById(TypeID);
            }
        }

        public int Create(Receipt receipt)
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.Create(receipt);
            }
        }

        public int Update(Receipt receiptId)
        {
            using (var receiptService = new ReceiptService())
            {
                return receiptService.Create(receiptId);
            }
        }
    }
}

