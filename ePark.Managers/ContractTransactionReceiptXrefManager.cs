﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class ContractTransactionReceiptXrefManager
    {
        public ContractTransactionReceiptXref GetById(int transactionId)
        {
            using (var contractTransactionXrefService = new ContractTransactionReceiptXrefService())
            {
                return contractTransactionXrefService.GetById(transactionId);
            }
        }

        public ContractTransactionReceiptXref GetByTransId(int transactionId)
        {
            using (var contractTransactionXrefService = new ContractTransactionReceiptXrefService())
            {
                return contractTransactionXrefService.GetByContractTransactionID(transactionId);
            }
        }

        public ContractTransactionReceiptXref GetReceiptXref(int transactionId)
        {
            using (var contractTransactionXrefService = new ContractTransactionReceiptXrefService())
            {
                return contractTransactionXrefService.GetByReceiptID(transactionId);
            }
        }

        public int Create(ContractTransactionReceiptXref transactionxref)
        {
            using (var contractTransactionXrefService = new ContractTransactionReceiptXrefService())
            {
                return contractTransactionXrefService.Create(transactionxref);
            }
        }

        public int Update(ContractTransactionReceiptXref transactionxref)
        {
            using (var contractTransactionXrefService = new ContractTransactionReceiptXrefService())
            {
                return contractTransactionXrefService.Update(transactionxref);
            }
        }

    }
}
