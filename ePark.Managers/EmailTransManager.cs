﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class EmailTransManager
    {
        public EmailTran GetById(int emailTransId)
        {
            using (var emailTransService = new EmailTransService())
            {
                return emailTransService.GetById(emailTransId);
            }
        }

        public List<EmailTran> GetList()
        {
            using (var emailTransService = new EmailTransService())
            {
                return emailTransService.GetList();
            }
        }
        public int Create(EmailTran parkingInquiry)
        {
            using (var emailTransService = new EmailTransService())
            {
                return emailTransService.Create(parkingInquiry);
            }
        }

        public int Update(EmailTran parkingInquiry)
        {
            using (var emailTransService = new EmailTransService())
            {
                return emailTransService.Update(parkingInquiry);
            }
        }


    }
}
