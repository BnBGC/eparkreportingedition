﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class AspNetUserProfileRolesViewManager
    {
        public vwAspNetUserProfileRole GetByUserId(int userId)
        {
            using (var aspNetUserProfileRolesViewService = new AspNetUserProfileRolesViewService())
            {
                return aspNetUserProfileRolesViewService.GetByUserId(userId);
            }
        }

        public List<vwAspNetUserProfileRole> GetList()
        {
            using (var aspNetUserProfileRolesViewService = new AspNetUserProfileRolesViewService())
            {
                return aspNetUserProfileRolesViewService.GetList();
            }
        }

    }
}
