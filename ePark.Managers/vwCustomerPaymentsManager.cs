﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class vwCustomerPaymentsManager
    {
        public List<vwCustomerPayment> GetList()
        {
            using (var custpayService = new vwCustomerPaymentService())
            {
                return custpayService.GetList();
            }
        }

        public List<vwCustomerPayment> GetByAspNetUserId(int id)
        {
            using (var custpayService = new vwCustomerPaymentService())
            {
                return custpayService.GetByAspNetUserId(id);
            }
        }

        public List<vwCustomerPayment> GetByContractId(int contractid)
        {
            using (var custpayService = new vwCustomerPaymentService())
            {
                return custpayService.GetByContractId(contractid);
            }
        }

        public List<vwCustomerPayment> GetByPaymentType(string paytype)
        {
            using (var custpayService = new vwCustomerPaymentService())
            {
                return custpayService.GetByPaymentType(paytype);
            }
        }

    }
}
