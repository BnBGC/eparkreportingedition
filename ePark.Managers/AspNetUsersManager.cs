﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class AspNetUsersManager
    {
        public AspNetUser GetById(int aspNetUserId)
        {
            using (var aspNetUsersService = new AspNetUsersService())
            {
                return aspNetUsersService.GetById(aspNetUserId);
            }
        }


        public AspNetUser GetBySECStamp(string secstamp)
        {
            using (var aspNetUsersService = new AspNetUsersService())
            {
                return aspNetUsersService.GetBySecurityStamp(secstamp);
            }
        }


        public AspNetUser GetByUserName(string username)
        {
            using (var aspNetUsersService = new AspNetUsersService())
            {
                return aspNetUsersService.GetByUserName(username);
            }
        }

        public AspNetUser GetBySecurityStamp(string securityStamp)
        {
            using (var aspNetUsersService = new AspNetUsersService())
            {
                return aspNetUsersService.GetBySecurityStamp(securityStamp);
            }
        }

        public List<AspNetUser> GetList()
        {
            using (var aspNetUsersService = new AspNetUsersService())
            {
                return aspNetUsersService.GetList();
            }
        }
        public int Create(AspNetUser aspNetUser)
        {
            using (var aspNetUsersService = new AspNetUsersService())
            {
                return aspNetUsersService.Create(aspNetUser);
            }
        }

        public int Update(AspNetUser aspNetUser)
        {
            using (var aspNetUsersService = new AspNetUsersService())
            {
                return aspNetUsersService.Update(aspNetUser);
            }
        }

    }
}
