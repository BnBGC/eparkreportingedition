﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class PaymentTransactionLogManager
    {

        public PaymentTransactionLog GetById(int transLogID)
        {
            using (var paymentTransactionLogsService = new PaymentTransactionLogsService())
            {
                return paymentTransactionLogsService.GetById(transLogID);
            }
        }

        public List<PaymentTransactionLog> GetList()
        {
            using (var paymentTransactionLogsService = new PaymentTransactionLogsService())
            {
                return paymentTransactionLogsService.GetList();
            }
        }
        public int Create(PaymentTransactionLog paymentTransactionLog)
        {
            using (var paymentTransactionLogsService = new PaymentTransactionLogsService())
            {
                return paymentTransactionLogsService.Create(paymentTransactionLog);
            }
        }

        public int Update(PaymentTransactionLog paymentTransactionLog)
        {
            using (var paymentTransactionLogService = new PaymentTransactionLogsService())
            {
                return paymentTransactionLogService.Update(paymentTransactionLog);
            }
        }

    }
}
