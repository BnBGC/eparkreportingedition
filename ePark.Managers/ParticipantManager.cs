﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;


namespace ePark.Managers
{
    public class ParticipantManager
    {
        public Participant GetById(int participantId)
        {
            using (var participantService = new ParticipantServices())
            {
                return participantService.GetById(participantId);
            }
        }

        public List<Participant> GetAll()
        {
            using (var participantService = new ParticipantServices())
            {
                return participantService.GetAll();
            }
        }


        public List<Participant> GetByContract(int contractId)
        {
            using (var participantServices = new ParticipantServices())
            {
                return participantServices.GetByContractId(contractId);
            }
        }


        public int Create(Participant participant)
        {
            using (var participantServices = new ParticipantServices())
            {
                return participantServices.Create(participant);
            }
        }

        public int Update(Participant participant)
        {
            using (var participantServices = new ParticipantServices())
            {
                return participantServices.Update(participant);
            }
        }

    }
}
