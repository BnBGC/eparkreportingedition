﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;


namespace ePark.Managers
{
    public class EmailStatusManager
    {

        public EmailStatu GetById(int emailStatusId)
        {
            using (var emailStatusService = new EmailStatusService())
            {
                return emailStatusService.GetById(emailStatusId);
            }
        }

        public List<EmailStatu> GetList()
        {
            using (var emailStatusService = new EmailStatusService())
            {
                return emailStatusService.GetList();
            }
        }
        public int Create(EmailStatu emailStatu)
        {
            using (var emailStatusService = new EmailStatusService())
            {
                return emailStatusService.Create(emailStatu);
            }
        }

        public int Update(EmailStatu emailStatu)
        {
            using (var emailStatusService = new EmailStatusService())
            {
                return emailStatusService.Update(emailStatu);
            }
        }
    }


}

