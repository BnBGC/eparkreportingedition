﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class GroupManager
    {
        public Group GetById(int groupId)
        {
            using (var groupService = new GroupService())
            {
                return groupService.GetById(groupId);
            }
        }

        public List<Group> GetList()
        {
            using (var groupService = new GroupService())
            {
                return groupService.GetList();
            }
        }
        public int Create(Group group)
        {
            using (var groupService = new GroupService())
            {
                return groupService.Create(group);
            }
        }

        public int Update(Group groupId)
        {
            using (var groupService = new GroupService())
            {
                return groupService.Create(groupId);
            }
        }
    }
}
