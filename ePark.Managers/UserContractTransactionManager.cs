﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
   public  class UserContractTransactionManager
    {

       public List<vwUserContractTransaction> GetTrans()
       {
           using (var reservationPaymentViewService = new UserContractTransactionService())
           {
               return reservationPaymentViewService.GetTrans();
           }
       }

       public List<vwUserContractTransaction> GetUnPaidContractTrans()
       {
           using (var reservationPaymentViewService = new UserContractTransactionService())
           {
               return reservationPaymentViewService.GetUnPaidContractTrans();
           }
       }


       public List<vwUserContractTransaction> GetPaidContractTrans()
       {
           using (var reservationPaymentViewService = new UserContractTransactionService())
           {
               return reservationPaymentViewService.GetPaidContractTrans();
           }
       }

    }
}
