﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class ReservationPaymentViewManager
    {
        public vwReservationPayment GetByUserId(int userId)
        {
            using (var reservationPaymentViewService = new ReservationPaymentViewService())
            {
                return reservationPaymentViewService.GetByReceiptId(userId);
            }
        }

        public List<vwReservationPayment> GetList()
        {
            using (var reservationPaymentViewService = new ReservationPaymentViewService())
            {
                return reservationPaymentViewService.GetList();
            }
        }

        public vwReservationPayment GetByContractTransactionId(int contracttransactionid)
        {
            using (var reservationPaymentViewService = new ReservationPaymentViewService())
            {
                return reservationPaymentViewService.GetByContractTransactionVwReservationPaymentId(contracttransactionid);
            }
        }



        public List<vwReservationPayment> GetOpenContractTransList()
        {
            using (var reservationPaymentViewService = new ReservationPaymentViewService())
            {
                return reservationPaymentViewService.GetOpenContractTrans();
            }
        }



        public List<vwReservationPayment> GetUnPaidContractTrans()
        {
            using (var reservationPaymentViewService = new ReservationPaymentViewService())
            {
                return reservationPaymentViewService.GetUnPaidContractTrans();
            }
        }

        public List<vwReservationPayment> GetUnPaidForUserContractTrans(int UserId)
        {
            using (var reservationPaymentViewService = new ReservationPaymentViewService())
            {
                return reservationPaymentViewService.GetUnPaidForUserContractTrans(UserId);
            }
        }


        public List<vwReservationPayment> GetPaidForUserContractTrans()
        {
            using (var reservationPaymentViewService = new ReservationPaymentViewService())
            {
                return reservationPaymentViewService.GetPaidContractTrans();
            }
        }

    }
}
