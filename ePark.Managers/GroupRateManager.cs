﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class GroupRateManager
    {

        public GroupRate GetByGroupRateId(int grouprateId)
        {
            using (var grouprateService = new GroupRateService())
            {
                return grouprateService.GetByGroupRateId(grouprateId);
            }
        }

        public GroupRate GetByGroupId(int groupId)
        {
            using (var grouprateService = new GroupRateService())
            {
                return grouprateService.GetByGroupId(groupId);
            }
        }

        public List<Group> GetList()
        {
            using (var groupService = new GroupService())
            {
                return groupService.GetList();
            }
        }
        public int Create(Group group)
        {
            using (var groupService = new GroupService())
            {
                return groupService.Create(group);
            }
        }

        public int Update(GroupRate groupRate)
        {
            using (var grouprateService = new GroupRateService())
            {
                return grouprateService.Update(groupRate);
            }
        }


    }
}
