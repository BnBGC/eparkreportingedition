﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class InquiryManager
    {
        public Inquiry GetById(int inquiryId)
        {
            using (var inquiryService = new InquiryService())
            {
                return inquiryService.GetById(inquiryId);
            }
        }

        public List<Inquiry> GetList()
        {
            using (var inquiryService = new InquiryService())
            {
                return inquiryService.GetList();
            }
        }

        public List<vwInquiry> GetViewList(int daysBack)
        {
            using (var inquiryService = new InquiryService())
            {
                return inquiryService.GetViewList(daysBack);
            }
        }

        //another method
        public List<vwInquiry> GetViewListByUserId(int userId)
        {
            using (var inquiryService = new InquiryService())
            {
                return inquiryService.GetViewListByUserId(userId);
            }
        }

        public List<vwInquiry> GetViewListByUserId(int userId, int daysBack)
        {
            using (var inquiryService = new InquiryService())
            {
                return inquiryService.GetViewListByUserId(userId, daysBack);
            }
        }

        public int Create(Inquiry parkingInquiry)
        {
            using (var inquiryService = new InquiryService())
            {
                return inquiryService.Create(parkingInquiry);
            }
        }

        public int Update(Inquiry parkingInquiry)
        {
            using (var inquiryService = new InquiryService())
            {
                return inquiryService.Update(parkingInquiry);
            }
        }
    }
}
