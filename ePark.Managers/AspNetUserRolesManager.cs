﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class AspNetUserRolesManager
    {

        public List<AspNetUserRole> GetUsersByRole(int aspNetUserRoleId)
        {
            using (var aspNetUserRolesService = new AspNetUserRolesService())
            {
                return aspNetUserRolesService.GetUsersByRole(aspNetUserRoleId);
            }
        }


        public List<AspNetUserRole> GetByRole(int aspNetUserRoleId)
        {
            using (var aspNetUserRolesService = new AspNetUserRolesService())
            {
                return aspNetUserRolesService.GetUsersByRole(aspNetUserRoleId);
            }
        }
        
        public int Create(AspNetUserRole aspNetUserRoleId)
        {
            using (var aspNetUserRolesService = new AspNetUserRolesService())
            {
                return aspNetUserRolesService.Create(aspNetUserRoleId);
            }
        }


        public int Update(AspNetUserRole aspNetUserRoleId)
        {
            using (var aspNetUserRolesService = new AspNetUserRolesService())
            {
                return aspNetUserRolesService.Update(aspNetUserRoleId);
            }
        }

    }
}
